#!/bin/bash
echo "WARNING: This will replace the test database with data from production!"
read -p "Are you sure (y/n)? " -r
echo    # (optional) move to a new line
if [[ $REPLY = "y" ]]
then
    ssh hagen.scrw.ca "sudo /usr/bin/mysqldump bolt" | sudo mysql bolt
fi
