from django.db import models

class Athlete(models.Model):
    GENDER_CHOICES = (('F', 'Female'),('M', 'Male'))
    name = models.CharField(max_length=64, unique=True)
    slug = models.SlugField(unique=True, help_text="Name in lower case, drop special characters, replace spaces with hyphens.")
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    dob = models.DateField(null=True, blank=True)
    joindate = models.DateField()
    quitdate = models.DateField(null=True, blank=True)
    stravaid = models.IntegerField(null=True, blank=True)
    isguest = models.BooleanField(default=False, db_index=True)
    def __str__(self):
        return self.name

class Athleteracename(models.Model):
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    name = models.CharField(max_length=64, unique=True)
    
class Config(models.Model):
    name = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=100)
    def __str__(self):
        return 'name={}, value={}'.format(self.name, self.value)

class Distance(models.Model):
    prename = models.CharField(max_length=25)
    name = models.CharField(max_length=25, unique=True)
    abbr = models.CharField(max_length=6)
    slug = models.SlugField(unique=True)
    km = models.DecimalField(max_digits=9, decimal_places=5)
    c25k = models.DecimalField(max_digits=12, decimal_places=10)
    ispb = models.BooleanField(default=False)
    def __str__(self):
        return self.name

class Raceseries(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)
    def __str__(self):
        return self.name

class Race(models.Model):
    series = models.ForeignKey(Raceseries, null=True, blank=True, default=None, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=True)
    shortname = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)
    def __str__(self):
        return self.name

class Samerace(models.Model):                                                        
    old_race = models.ForeignKey(Race, related_name='old_race', on_delete=models.CASCADE)                  
    current_race = models.ForeignKey(Race, related_name='current_race', on_delete=models.CASCADE)          

class Category(models.Model):
    name = models.CharField(max_length=20, unique=True)
    ismasters = models.BooleanField(default=False)
    def __str__(self):
        return self.name

class Event(models.Model):
    race = models.ForeignKey(Race, on_delete=models.CASCADE)
    distance = models.ForeignKey(Distance, on_delete=models.CASCADE)
    date = models.DateField(db_index=True)
    city = models.CharField(max_length=50)
    resultsurl = models.URLField(max_length=500, null=True, blank=True)
    hasresults = models.BooleanField(default=False)
    class Meta:
        unique_together = ('race', 'distance', 'date')
    def __str__(self):
        return '{} {} {}'.format(self.date.year, self.race, self.distance)

class Result(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    bib = models.CharField(max_length=6, blank=True)
    city = models.CharField(max_length=50)
    place = models.IntegerField()
    gender_place = models.IntegerField(null=True, blank=True)
    gender_total = models.IntegerField(null=True, blank=True)
    category_place = models.IntegerField(null=True, blank=True)
    category_total = models.IntegerField(null=True, blank=True)
    chiptime = models.DurationField(null=True)
    guntime = models.DurationField(null=True)
    fivek_equiv = models.DurationField(null=True)
    class Meta:
        unique_together = ('event', 'place')
    def __str__(self):
        return str((self.event, self.bib))
#    def _get_fivek_equiv(self):
#       "Returns the 5K equivalent time"
#       c25kdict = Distance.objects.get(id=self.event.distance.id).c25k
#       return self.chiptime * 2
#    fivek_equiv = property(_get_fivek_equiv)

class Resultexclude(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    guntime = models.DurationField()

class Agefilter(models.Model):
    name = models.CharField(max_length=32)
    slug = models.SlugField(unique=True)
    minage = models.IntegerField()
    maxage = models.IntegerField()
    sortorder = models.IntegerField()

class Stravasegment(models.Model):
    name = models.CharField(max_length=128)
    distance = models.DecimalField(max_digits=9, decimal_places=3)
    average_grade = models.DecimalField(max_digits=3, decimal_places=1)
    effort_count = models.IntegerField()
    athlete_count = models.IntegerField()
    istopx = models.NullBooleanField(null=True, default=None)

class Stravaleaderboard(models.Model):
    GENDER_CHOICES = (('F', 'Female'),('M', 'Male'))
    stravasegment = models.ForeignKey(Stravasegment, on_delete=models.CASCADE)
    rank = models.IntegerField()
    athlete_id = models.IntegerField()
    athlete_name = models.CharField(max_length=64)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    seconds = models.IntegerField()

class Endurteam(models.Model):
    year = models.IntegerField()
    name = models.CharField(max_length=128, help_text="Match previous-year same team name exactly.")
    slug = models.SlugField(help_text="Name in lower case, drop special characters, replace spaces with hyphens.")
    st1 = models.ForeignKey(Athlete, related_name='st1', null=True, blank=True, on_delete=models.CASCADE)
    st2 = models.ForeignKey(Athlete, related_name='st2', null=True, blank=True, on_delete=models.CASCADE)
    st3 = models.ForeignKey(Athlete, related_name='st3', null=True, blank=True, on_delete=models.CASCADE)
    st4 = models.ForeignKey(Athlete, related_name='st4', null=True, blank=True, on_delete=models.CASCADE)
    st5 = models.ForeignKey(Athlete, related_name='st5', null=True, blank=True, on_delete=models.CASCADE)
    st6 = models.ForeignKey(Athlete, related_name='st6', null=True, blank=True, on_delete=models.CASCADE)
    st7 = models.ForeignKey(Athlete, related_name='st7', null=True, blank=True, on_delete=models.CASCADE)
    def __str__(self):
        return '{} {}'.format(self.year, self.name)

class Endurathletebattle(models.Model):
    battle_year = models.IntegerField()
    athlete1_year = models.IntegerField()
    athlete1 = models.ForeignKey(Athlete, related_name='athlete1', on_delete=models.CASCADE)
    athlete2_year = models.IntegerField()
    athlete2 = models.ForeignKey(Athlete, related_name='athlete2', on_delete=models.CASCADE)
    sortorder = models.IntegerField(default=1)

class Endurteambattle(models.Model):
    battle_year = models.IntegerField()
    team1 = models.ForeignKey(Endurteam, related_name='team1', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Endurteam, related_name='team2', on_delete=models.CASCADE)
    sortorder = models.IntegerField(default=1)

class Endurteamsuperbattle(models.Model):
    battle_year = models.IntegerField()
    team1_name = models.CharField(max_length=128)
    team1_slug = models.SlugField(help_text="Name in lower case, drop special characters, replace spaces with hyphens.")
    teams1 = models.ManyToManyField(Endurteam, related_name='teams1', blank=True)
    team2_name = models.CharField(max_length=128)
    team2_slug = models.SlugField(help_text="Name in lower case, drop special characters, replace spaces with hyphens.")
    teams2 = models.ManyToManyField(Endurteam, related_name='teams2', blank=True)
    sortorder = models.IntegerField(default=1)

class Challenge(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    slug = models.SlugField()
    count = models.IntegerField()   # number of athletes to count
    def __str__(self):
        return '{} - {}'.format(self.event, self.name)

class Challengeteam(models.Model):
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    slug = models.SlugField()
    def __str__(self):
        return '{} - {}'.format(self.challenge, self.name)

class Challengemember(models.Model):
    challengeteam = models.ForeignKey(Challengeteam, on_delete=models.CASCADE)
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    def __str__(self):
        return '{} - {}'.format(self.challengeteam, self.athlete)

class Split(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    place = models.IntegerField()
    split_num = models.IntegerField()
    split_time = models.DurationField(null=True)
    class Meta:
        unique_together = ('event', 'place', 'split_num')

class Standarddef(models.Model):
    GENDER_CHOICES = (('F', 'Female'),('M', 'Male'))
    LETTER_CHOICES = (('A', 'A'),('B', 'B'),('C', 'C'))
    distance = models.ForeignKey(Distance, on_delete=models.CASCADE)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    ismasters = models.BooleanField()
    letter = models.CharField(max_length=1, choices=LETTER_CHOICES)
    time = models.DurationField()
    def __str__(self):
        masters = 'Any Age'
        if self.ismasters:
            masters = 'Masters'
        return '{} {} {} {}'.format(self.letter, self.distance, self.gender, masters)

class Standardmanual(models.Model):
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    standarddef = models.ForeignKey(Standarddef, on_delete=models.CASCADE)
    ispb = models.BooleanField()

class Standardcalc(models.Model):
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    standarddef = models.ForeignKey(Standarddef, on_delete=models.CASCADE)
    ispb = models.BooleanField()

class Indexcalc(models.Model):
    value = models.TextField()

class Multisport(models.Model):
    date = models.DateField()
    event_type = models.CharField(max_length=100)
    distance = models.CharField(max_length=100)
    race = models.CharField(max_length=200)
    resultsurl = models.URLField(max_length=500, null=True, blank=True)
    athlete = models.ForeignKey(Athlete, on_delete=models.CASCADE)
    time = models.DurationField()
    place = models.IntegerField()
    finishers = models.IntegerField()
    gender_place = models.IntegerField()
    gender_finishers = models.IntegerField()
    category = models.CharField(max_length=100)
    category_place = models.IntegerField(null=True, blank=True)
    category_finishers = models.IntegerField(null=True, blank=True)
    bike_type = models.CharField(max_length=100, blank=True)
    event_slug = models.CharField(max_length=310, db_index=True)
    def _get_os(self):
       "Returns overall score"
       return (1 - (self.place / self.finishers)) * 100
    def _get_gs(self):
       "Returns gender score"
       return (1 - (self.gender_place / self.gender_finishers)) * 100
    os = property(_get_os)
    gs = property(_get_gs)
