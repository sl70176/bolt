from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date
import urllib
from ..models import *
from ..utils import get_wrdict

def index(request):
    config_dict = dict(Config.objects.values_list('name', 'value'))
    events = []
    named_event = namedtuple('ne', ['frequency',
                                    'event',
                                    'd2e',
                                   ])
    event_count_dict = {}
    asofdate = date.today()
    wrdict = get_wrdict(asofdate, 'FM', 'any-age')
    for k, v in wrdict.items():
        for e in v.events:
            if e in event_count_dict:
                event_count_dict[e] += 1
            else:
                event_count_dict[e] = 1
    for k, v in event_count_dict.items():
        if v == 1:
            continue
        d2e = int(config_dict['d2e']) - ((date.today() - k.date)).days
        events.append(named_event(v, k, d2e))
    events = sorted(events, key=attrgetter('frequency', 'd2e'), reverse=True)
    context = {'events': events,
              }
    return render(request, 'boltapp/rankiest.html', context)
