from django.shortcuts import render
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
from ..models import (
    Athlete,
    Challenge,
    Challengemember,
    Challengeteam,
    Config,
    Distance,
    Event,
    Result,
)
from ..utils import (
    get_5k_equiv,
    get_pace,
    get_wrdict,
)


def index(request, year, race_slug, distance_slug, challenge_slug):
    today = date.today()
    wrdict = get_wrdict(today, "FM", "any-age", alltime=False)
    distance = Distance.objects.get(slug=distance_slug)
    config_dict = dict(Config.objects.values_list("name", "value"))
    female_colour_class = config_dict["female_colour_class"]
    male_colour_class = config_dict["male_colour_class"]
    event = Event.objects.get(
        race__slug=race_slug, distance__slug=distance_slug, date__icontains=year
    )
    dist_pbs = dict(
        Athlete.objects.filter(
            result__event__distance=distance, result__event__date__lt=event.date
        )
        .annotate(min_chiptime=Min("result__chiptime"))
        .values_list("id", "min_chiptime")
    )
    fivek_equiv_pbs = dict(
        Athlete.objects.filter(result__event__date__lt=event.date)
        .annotate(min_fivek_equiv=Min("result__fivek_equiv"))
        .values_list("id", "min_fivek_equiv")
    )
    dbresults = Result.objects.filter(event=event).order_by("chiptime")
    challenge = Challenge.objects.get(event=event, slug=challenge_slug)
    teams = Challengeteam.objects.filter(challenge=challenge)
    teamresults = []
    namedteamresult = namedtuple(
        "ntr", ["team", "total_time", "average_time", "average_place", "results"]
    )
    namedresult = namedtuple(
        "nr",
        [
            "athlete",
            "result",
            "pace",
            "fivek_equiv",
            "colour_class",
            "glyph",
            "rank",
            "ranktime",
        ],
    )
    namedwr = namedtuple("nwr", ["rank"])
    for team in teams:
        time_results = []
        notime_results = []
        results = []
        total_time = timedelta(seconds=0)
        members = Challengemember.objects.filter(challengeteam=team)
        for m in members:
            glyph = False
            if m.athlete.gender == "F":
                colour_class = female_colour_class
            else:
                colour_class = male_colour_class
            try:
                thisresult = dbresults.filter(athlete=m.athlete)[0]
            except Exception:
                thisresult = None
                pace = False
                fivek_equiv = ""
            else:
                pace = get_pace(thisresult.chiptime, event.distance.km)
                fivek_equiv = get_5k_equiv(thisresult.chiptime, event.distance.c25k)
                if m.athlete.id in dist_pbs:
                    if thisresult.chiptime < dist_pbs[m.athlete.id]:
                        glyph = "flash"
                else:
                    glyph = "flash"
                if m.athlete.id in fivek_equiv_pbs:
                    if fivek_equiv < fivek_equiv_pbs[m.athlete.id]:
                        glyph = "star"
                else:
                    glyph = "star"
                fivek_equiv = str(fivek_equiv).lstrip("0:")
            try:
                rank = wrdict[m.athlete]
            except Exception:
                rank = namedwr(9999)
                ranktime = ""
            else:
                ranktime = str(timedelta(seconds=round(rank.avgaseconds))).lstrip("0:")
            if thisresult:
                time_results.append(
                    namedresult(
                        m.athlete,
                        thisresult,
                        pace,
                        fivek_equiv,
                        colour_class,
                        glyph,
                        rank,
                        ranktime,
                    )
                )
            else:
                notime_results.append(
                    namedresult(
                        m.athlete,
                        thisresult,
                        pace,
                        fivek_equiv,
                        colour_class,
                        glyph,
                        rank,
                        ranktime,
                    )
                )
        average_time = total_time / challenge.count
        notime_results = sorted(notime_results, key=attrgetter("rank"))
        time_results = sorted(time_results, key=attrgetter("result.chiptime"))
        results = time_results + notime_results
        if len(time_results) >= challenge.count:
            total_time = timedelta(seconds=0)
            total_place = 0
            for i in range(0, challenge.count):
                total_time += time_results[i].result.chiptime
                total_place += time_results[i].result.place
            average_time = total_time / challenge.count
            average_time = str(average_time).rstrip("0")
            average_place = total_place / challenge.count
            if average_time[-1] == ".":
                average_time += "0"
            atparts = average_time.split(".")
            if len(atparts) > 1:
                average_time = atparts[0] + "." + atparts[1][0]
            else:
                average_time = atparts[0] + ".0"
        else:
            total_time = False
            average_time = False
            average_place = False
        teamresults.append(namedteamresult(team, total_time, average_time, average_place, results))
    try:
        teamresults = sorted(teamresults, key=attrgetter("total_time"))
    except Exception:
        pass
    config_dict = dict(Config.objects.values_list("name", "value"))
    female_colour_class = config_dict["female_colour_class"]
    male_colour_class = config_dict["male_colour_class"]
    results = []
    context = {
        "event": event,
        "challenge": challenge,
        "teamresults": teamresults,
        "female_colour_class": female_colour_class,
        "male_colour_class": male_colour_class,
    }
    # print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, "boltapp/challenge.html", context)
