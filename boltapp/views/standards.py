from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    year = False
    gender = 'FM'
    if 'year' in qstring:
        year = qstring['year'][0]
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    std_colour_dict = {'A': config_dict['standard_a_colour_class'],
                       'B': config_dict['standard_b_colour_class'],
                       'C': config_dict['standard_c_colour_class']}
    gender_colour_class = False
    dbresults = Standardcalc.objects.all().order_by('-event__date')
    dates = dbresults.values_list('event__date', flat=True).distinct()
    years = sorted(set([ x.year for x in dates ]), reverse=True)
    if year:
        dbresults = dbresults.filter(event__date__icontains=year)
    if gender != 'FM':
        dbresults = dbresults.filter(athlete__gender=gender)
    namedresult = namedtuple('nr', ['athlete',
                                    'gender_colour_class',
                                    'letter',
                                    'letter_colour_class',
                                    'distance',
                                    'event'])
    results = []
    for r in dbresults:
        athlete = r.athlete
        if r.athlete.gender == 'F':
            gender_colour_class = config_dict['female_colour_class']
        else:
            gender_colour_class = config_dict['male_colour_class']
        letter = r.standarddef.letter
        letter_colour_class = std_colour_dict[letter]
        distance = r.standarddef.distance.name
        if r.ispb:
            distance += ' (PB)'
        elif r.standarddef.ismasters:
            distance += ' (Masters)'
        event = r.event
        results.append(namedresult(athlete,
                                   gender_colour_class,
                                   letter,
                                   letter_colour_class,
                                   distance, 
                                   event))

    named_standard = namedtuple('ns', ['distance', 'fa', 'fb', 'fc', 'ma', 'mb', 'mc'])
    oa_standards = []
    for d in ['5-km', '10-km', 'half-marathon', 'marathon']:
        thisdist = Distance.objects.get(slug=d)
        this_stds = named_standard(thisdist,
                                    Standarddef.objects.get(distance=thisdist,gender='F',ismasters=False,letter='A').time,
                                    Standarddef.objects.get(distance=thisdist,gender='F',ismasters=False,letter='B').time,
                                    Standarddef.objects.get(distance=thisdist,gender='F',ismasters=False,letter='C').time,
                                    Standarddef.objects.get(distance=thisdist,gender='M',ismasters=False,letter='A').time,
                                    Standarddef.objects.get(distance=thisdist,gender='M',ismasters=False,letter='B').time,
                                    Standarddef.objects.get(distance=thisdist,gender='M',ismasters=False,letter='C').time)
        oa_standards.append(this_stds)
    masters_standards = []
    for d in ['5-km', '10-km', 'half-marathon', 'marathon']:
        thisdist = Distance.objects.get(slug=d)
        this_stds = named_standard(thisdist,
                                    Standarddef.objects.get(distance=thisdist,gender='F',ismasters=True,letter='A').time,
                                    Standarddef.objects.get(distance=thisdist,gender='F',ismasters=True,letter='B').time,
                                    Standarddef.objects.get(distance=thisdist,gender='F',ismasters=True,letter='C').time,
                                    Standarddef.objects.get(distance=thisdist,gender='M',ismasters=True,letter='A').time,
                                    Standarddef.objects.get(distance=thisdist,gender='M',ismasters=True,letter='B').time,
                                    Standarddef.objects.get(distance=thisdist,gender='M',ismasters=True,letter='C').time)
        masters_standards.append(this_stds)

    context = {'results': results,
               'year': year,
               'years': years,
               'gender': gender,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class,
               'oa_standards': oa_standards,
               'masters_standards': masters_standards}
    return render(request, 'boltapp/standards.html', context)
