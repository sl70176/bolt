from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date
import urllib
from ..models import *
from ..utils import get_races

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    dates = 'Past'
    distance = False
    if 'dates' in qstring:
        dates = qstring['dates'][0]
    if 'distance' in qstring:
        rawdistance = qstring['distance'][0]
        distance = Distance.objects.get(slug=rawdistance)
    races = get_races(dates)
    if distance:
        newraces = []
        for r in races:
            for e in r.events:
                if e.distance == distance:
                    newraces.append(r)
        races = newraces
    distances = Distance.objects.filter(ispb=True).exclude(slug='2_2-km').order_by('km')
    context = {'races': races,
               'dates': dates,
               'distance': distance,
               'distances': distances}
    return render(request, 'boltapp/races.html', context)
    #print(len(db.connection.queries))   # number of sql queries that happened
