from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, year1, team1_slug, year2, team2_slug):
    #qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    namedresult = namedtuple('nr', ['event',
                                    'distance',
                                    'stage',
                                    'athlete',
                                    'guntime',
                                    'pace'])
    teams = []
    teams.append([Endurteam.objects.get(year=year1, slug=team1_slug), year1])
    teams.append([Endurteam.objects.get(year=year2, slug=team2_slug), year2])
    years = [year1, year2]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    endurbattle_colour_class1 = config_dict['endurbattle_colour_class1']
    endurbattle_colour_class2 = config_dict['endurbattle_colour_class2']
    endurstages = Config.objects.filter(name__icontains='endurstage')
    year1_date = date(int(year1), 7, 31)
    year1_ranking = get_wrdict(year1_date, 'FM', 'any-age')
    year2_date = date(int(year2), 7, 31)
    year2_ranking = get_wrdict(year2_date, 'FM', 'any-age')
    stage_distances = {}
    for stage in range(1, 8):
        distance_slug = endurstages.get(value=stage).name.split('_', 1)[1]
        distance = Distance.objects.get(slug=distance_slug)
        stage_distances[stage] = distance
    results = []
    for t in teams:
        dbresults = Result.objects.filter(event__date__icontains=t[1], event__race__name__icontains='endurrun')
        team_results = []
        for i in range(1,8):
            ath_expr = 't[0].st{}'.format(i)
            athlete = eval(ath_expr)
            distance = stage_distances[i]
            try:
                result = dbresults.get(event__distance=distance, athlete=athlete)
            except:
                result = False
            if result:
                event = result.event
                distance = event.distance
                guntime = (result.guntime -
                           timedelta(microseconds=result.guntime.microseconds))
                pace = get_pace(guntime, event.distance.km)
            else:
                event = False
                guntime = ''
                pace = ''

            stage = config_dict['endurstage_{}'.format(stage_distances[i].slug)]
            team_results.append(namedresult(event,
                                       distance,
                                       stage,
                                       athlete,
                                       guntime,
                                       pace))
        results.append(team_results)
    named_combined_result = namedtuple('ncr', ['stage', 'distance', 'athlete1', 'athlete1rank', 'event1', 'guntime1', 'pace1', 'athlete2', 'athlete2rank', 'event2', 'guntime2', 'pace2', 'stage_gap', 'stage_gap_colour_class','stage_total_gap', 'stage_total_gap_colour_class'])
    combined_results = []
    total_gap = timedelta(seconds=0)
    kill_total_gap = False
    for i in range(0, 7):
        distance = stage_distances[i+1]
        athlete1 = results[0][i].athlete
        athlete2 = results[1][i].athlete
        try:
            stage = results[0][i].stage
            event1 = results[0][i].event
            guntime1 = results[0][i].guntime
            pace1 = results[0][i].pace
        except:
            stage = i + 1
            event1 = False
            guntime1 = False
            pace1 = False
        try:
            event2 = results[1][i].event
            guntime2 = results[1][i].guntime
            pace2 = results[1][i].pace
        except:
            event2 = False
            guntime2 = False
            pace2 = False
        if event1 and not event2:
            kill_total_gap = True
        if not event1 and event2:
            kill_total_gap = True
        if event1 and event2:
            raw_stage_gap = guntime1 - guntime2
            total_gap += raw_stage_gap
            stage_gap_colour_class = False
            stage_total_gap_colour_class = False
            if raw_stage_gap == timedelta(seconds=0):
                stage_gap_colour_class = 'default'
                stage_gap = timedelta(seconds=0)
            elif raw_stage_gap > timedelta(seconds=0):
                stage_gap_colour_class = endurbattle_colour_class2
                stage_gap = raw_stage_gap
            else:
                stage_gap_colour_class = endurbattle_colour_class1
                stage_gap = abs(raw_stage_gap)
            if total_gap == timedelta(seconds=0):
                stage_total_gap_colour_class = 'default'
                stage_total_gap = timedelta(seconds=0)
            elif total_gap > timedelta(seconds=0):
                stage_total_gap_colour_class = endurbattle_colour_class2
                stage_total_gap = total_gap
            else:
                stage_total_gap_colour_class = endurbattle_colour_class1
                stage_total_gap = abs(total_gap)
        else:
            stage_gap = False
            stage_gap_colour_class = False
            stage_total_gap = False
            stage_total_gap_colour_class = False
        if kill_total_gap:
            stage_total_gap = False
        athlete1rank = athlete2rank = False
        if athlete1 in year1_ranking:
            athlete1rank = year1_ranking[athlete1].rank
        if athlete2 in year2_ranking:
            athlete2rank = year2_ranking[athlete2].rank
        combined_results.append(named_combined_result(stage, distance, athlete1, athlete1rank, event1, guntime1, pace1,
                                                      athlete2, athlete2rank, event2, guntime2, pace2, stage_gap,
                                                      stage_gap_colour_class, stage_total_gap,
                                                      stage_total_gap_colour_class))

    context = {'results': combined_results,
               'teams': teams,
               'years': years,
               'endurbattle_colour_class1': endurbattle_colour_class1,
               'endurbattle_colour_class2': endurbattle_colour_class2}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/endurrun_battle_team.html', context)
