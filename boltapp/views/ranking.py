from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta, datetime
import urllib
import calendar
from ..models import *
from ..utils import *

named_dropdown_filter = namedtuple('nyf', ['current_value', 'btn_class', 'items'])
named_item = namedtuple('ni', ['value', 'url'])

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    qsgender = 'FM'
    if 'gender' in qstring:
        qsgender = qstring['gender'][0]
    qsage = 'any-age'
    if 'age' in qstring:
        qsage = qstring['age'][0]
    asofdate = date.today()
    alltime = False
    qsdate = asofdate
    if 'date' in qstring:
        qsdate = qstring['date'][0]
        if qsdate != 'all-time':
            asofdate = datetime.strptime(qsdate, '%Y-%m-%d').date()
        else:
            alltime = True
    year = asofdate.year
    monthnum = int(asofdate.strftime('%m'))    
    firstday = datetime.strptime(Config.objects.get(name='firstday').value, "%Y-%m-%d").date()
    if asofdate >= firstday:
        wrdict1 = get_wrdict(asofdate, qsgender, qsage, alltime, getstd=True)
    else:
        wrdict1 = {}
    prevdate = getprevmonthlastday(year, monthnum)    
    if prevdate >= firstday:
        wrdict2 = get_wrdict(prevdate, qsgender, qsage, alltime)
    else:
        wrdict2 = {}
    wrlist = wrdict2list(wrdict1, wrdict2, alltime)
    archives, currentyear = getarchives()
    date_filter = make_date_filter(asofdate, qsgender, qsage, archives, alltime)
    gender_filter = make_gender_filter(qsdate, qsgender, qsage)
    age_filter = make_age_filter(qsage, qsdate, qsgender)
    d2dred = int(Config.objects.get(name='d2dred').value)
    context = {'asofdate': asofdate,
               'wrlist': wrlist,
               'alltime': alltime,
               'date_filter': date_filter,
               'gender_filter': gender_filter,
               'age_filter': age_filter,
               'd2dred': d2dred}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/ranking.html', context)

def getarchives():
    namedarchive = namedtuple('na', ['year', 'month', 'asofdate'])
    currentyear = int(date.today().strftime("%Y"))
    currentmonth = int(date.today().strftime("%m"))
    dbfirstday = Config.objects.get(name='firstday').value
    firstday = datetime.strptime(dbfirstday, '%Y-%m-%d').date()
    loopyear = int(firstday.year)
    loopmonth = int(firstday.strftime("%m"))
    archives = []
    while ((loopyear * 100) + loopmonth) < ((currentyear * 100) + currentmonth):
        if len(str(loopmonth)) == 1:
            fullloopmonth = '0{}'.format(loopmonth)
        else:
            fullloopmonth = str(loopmonth)
        day = calendar.monthrange(loopyear, loopmonth)[1]
        asofdate = '{}-{}-{}'.format(loopyear, fullloopmonth, day)
        asofdate = date(loopyear, loopmonth, day)
        monthname = monthnum2name(loopmonth)
        archives.append(namedarchive(loopyear, monthname, asofdate))
        if loopmonth == 12:
            loopmonth = 1
            loopyear += 1
        else:
            loopmonth += 1
    archives = list(reversed(archives))
    pruned_archives = []
    count = 1
    for i in archives:
        if count < 4:
            pruned_archives.append(i)
        else:
            if i.month == 'January':
                pruned_archives.append(i)
        count += 1
    return list(pruned_archives), currentyear

def make_date_filter(qsdate, qsgender, qsage, archives, alltime):
    qs_params = []
    if qsgender != 'FM':
        qs_params.append('gender={}'.format(qsgender))
    if qsage != 'any-age':
        qs_params.append('age={}'.format(qsage))
    items = []
    if qsdate == date.today():
        if alltime:
            current_value = 'All-Time'
            current_qs = '/?' + '&'.join(sorted(qs_params))
            if current_qs == '/?':
                current_qs = ''
            items.append(named_item('Current', '/ranking{}'.format(current_qs)))
        else:
            current_value = 'Current'
            alltime_params = list(qs_params)
            alltime_params.append('date=all-time')
            alltime_qs = '/?' + '&'.join(sorted(alltime_params))
            items.append(named_item('All-Time', '/ranking{}'.format(alltime_qs)))
    else:
        archive_item = [ x for x in archives if x.asofdate == qsdate ]
        if len(archive_item) == 0:
            current_value = qsdate
        else:
            current_value = '{} {}'.format(archive_item[0].month,
                                           archive_item[0].year)

        current_qs = '/?' + '&'.join(sorted(qs_params))
        if current_qs == '/?':
            current_qs = ''
        items.append(named_item('Current', '/ranking{}'.format(current_qs)))
        alltime_params = list(qs_params)
        alltime_params.append('date=all-time')
        alltime_qs = '/?' + '&'.join(sorted(alltime_params))
        items.append(named_item('All-Time', '/ranking{}'.format(alltime_qs)))
    btn_class = 'default'
    for i in archives:
        if i.asofdate != qsdate:
            this_params = list(qs_params)
            this_params.append('date={}'.format(i.asofdate))
            this_qs = '/?' + '&'.join(sorted(this_params))
            items.append(named_item('{} {}'.format(i.month, i.year), '/ranking{}'.format(this_qs)))
    date_filter = named_dropdown_filter(current_value, btn_class, items)
    return date_filter

def make_gender_filter(qsdate, qsgender, qsage):
    named_button = namedtuple('nb', ['value', 'url',
                                     'colour_class', 'disabled'])
    female_colour_class = Config.objects.get(name='female_colour_class').value
    male_colour_class = Config.objects.get(name='male_colour_class').value
    gender_filter = []
    qs_params = []
    if qsdate != date.today():
        qs_params.append('date={}'.format(qsdate))
    if qsage != 'any-age':
        qs_params.append('age={}'.format(qsage))
    # FM
    if qsgender == 'FM':
        disabled = 'disabled'
    else:
        disabled = ''
    new_params = '/?' + '&'.join(sorted(qs_params))
    if new_params == '/?':
        new_params = ''
    gender_filter.append(named_button(False, '/ranking{}'.format(new_params), 'default', disabled))
    # F
    if qsgender == 'F':
        disabled = 'disabled'
        f_params = list(qs_params)
    else:
        disabled = ''
        f_params = qs_params + ['gender=F',]
    new_params = '?' + '&'.join(sorted(f_params))
    gender_filter.append(named_button('F', '/ranking/{}'.format(new_params), female_colour_class, disabled))
    # M
    if qsgender == 'M':
        disabled = 'disabled'
        m_params = list(qs_params)
    else:
        disabled = ''
        m_params = qs_params + ['gender=M',]
    new_params = '?' + '&'.join(sorted(m_params))
    gender_filter.append(named_button('M', '/ranking/{}'.format(new_params), male_colour_class, disabled))
    return gender_filter

def make_age_filter(qsage, qsdate, qsgender):
    btn_class = 'default'
    qs_params = []
    items = []
    if qsgender != 'FM':
        qs_params.append('gender={}'.format(qsgender))
    if qsdate  != date.today():
        qs_params.append('date={}'.format(qsdate))
    for af in Agefilter.objects.all().order_by('sortorder'):
        if af.slug == 'any-age':
            age_qs = '/?' + '&'.join(sorted(qs_params))
            if age_qs == '/?':
                age_qs = ''
            if af.slug != qsage:
                items.append(named_item(af.name, '/ranking{}'.format(age_qs)))
        else:
            age_params =  ['age={}'.format(af.slug),] + qs_params
            age_qs = '/?' + '&'.join(sorted(age_params))
            if af.slug != qsage:
                items.append(named_item(af.name, '/ranking{}'.format(age_qs)))
    current_value = Agefilter.objects.get(slug=qsage).name
    age_filter = named_dropdown_filter(current_value, btn_class, items)
    return age_filter
