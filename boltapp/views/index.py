from django.shortcuts import render
from django.http import HttpResponse
from collections import namedtuple
from datetime import date
from ..models import *
from ..utils import *
from ..views import quad
from operator import attrgetter

named_top5 = namedtuple('nt', ['female_athlete',  'male_athlete'])

def index(request):
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    top5 = get_top5()
    top5alltime = get_top5(alltime=True)
    recent_pbs = get_recent_pbs(5)
    recent_races = get_races('Past')[:5]
    upcoming_races = get_races('Future')[:5]
    quad_top5 = get_quad_top5()
    context = {'top5': top5,
               'top5alltime': top5alltime,
               'recent_pbs': recent_pbs,
               'recent_races': recent_races,
               'upcoming_races': upcoming_races,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class,
               'quad_top5': quad_top5}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/index.html', context)

def get_top5(alltime=False):
    wrdict = get_wrdict(date.today(), 'FM', 'any-age', alltime=alltime) 
    wrlist = wrdict2list(wrdict, False, alltime)
    female_top5 = []
    count = 0
    for i in wrlist:
        if count == 5:
            break
        if i.athlete.gender == 'F':
            female_top5.append(i)
            count += 1
    male_top5 = []
    count = 0
    for i in wrlist:
        if count == 5:
            break
        if i.athlete.gender == 'M':
            male_top5.append(i)
            count += 1
    top5 = []
    for j in range(0,5):
        top5.append(named_top5(female_top5[j], male_top5[j]))
    return top5

def get_quad_top5():
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    results =  quad.get_results('FM', female_colour_class, male_colour_class)
    female_top5 = []
    count = 0
    for i in results:
        if count == 5:
            break
        if i.athlete.gender == 'F':
            female_top5.append(i)
            count += 1
    male_top5 = []
    count = 0
    for i in results:
        if count == 5:
            break
        if i.athlete.gender == 'M':
            male_top5.append(i)
            count += 1
    quad_top5 = []
    for j in range(0,5):
        quad_top5.append(named_top5(female_top5[j], male_top5[j]))
    return quad_top5
