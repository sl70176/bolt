from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, year, athlete_slug):
    #qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    namedresult = namedtuple('nr', ['event',
                                    'stage',
                                    'guntime',
                                    'pace',
                                    'fivek_equiv',
                                    'place',
                                    'gender_place',
                                    'category_place',
                                    'category'])
    athlete = Athlete.objects.get(slug=athlete_slug, isguest=False)
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    if athlete.gender == 'F':
        gender_colour_class = config_dict['female_colour_class']
    else:
        gender_colour_class = config_dict['male_colour_class']
    dbresults = Result.objects.filter(athlete_id=athlete.id, event__date__icontains=year, event__race__name__icontains='endurrun').order_by('event__date')
    if athlete.quitdate:
        dbresults = dbresults.filter(event__date__lte=athlete.quitdate)
    results = []
    for result in dbresults:
    # Skip athletes who quit before this race
        event = result.event
        guntime = (result.guntime - 
                   timedelta(microseconds=result.guntime.microseconds))
        pace = get_pace(guntime, event.distance.km)
        fivek_equiv = str(get_5k_equiv(guntime, event.distance.c25k)).lstrip('0:')
        if result.gender_place is not None:
            gender_place = result.gender_place
        else:
            gender_place = '' 
        if result.category_place is not None:
            category_place = result.category_place
        else:
            category_place = '' 
        stage = config_dict['endurstage_{}'.format(event.distance.slug)]
        results.append(namedresult(event,
                                   stage,
                                   guntime,
                                   pace,
                                   fivek_equiv,
                                   result.place,
                                   gender_place,
                                   category_place,
                                   result.category.name))
    context = {'results': results,
               'athlete': athlete,
               'year': year,
               'gender_colour_class': gender_colour_class}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/endurrun_athlete.html', context)
