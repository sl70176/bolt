from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    year = False
    gender = 'FM'
    if 'year' in qstring:
        year = qstring['year'][0]
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    gender_colour_class = False
    dbresults = (Result.objects
                       .filter(athlete__isguest=False,
                               gender_place=1)
                       .order_by('-event__date', '-event__distance__km'))
    dates = dbresults.order_by('-event__date').values_list('event__date', flat=True).distinct()
    years = sorted(set([ x.year for x in dates ]), reverse=True)
    if year:
        dbresults = dbresults.filter(event__date__icontains=year)
    if gender != 'FM':
        dbresults = dbresults.filter(athlete__gender=gender)
    namedresult = namedtuple('nr', ['result',
                                    'guntime',
                                    'pace',
                                    'gender_colour_class'])
    results = []
    for r in dbresults:
        if r.athlete.quitdate:
            if r.event.date > r.athlete.quitdate:
                continue
        guntime = r.guntime - timedelta(microseconds=r.guntime.microseconds)
        pace = get_pace(guntime, r.event.distance.km)
        if r.athlete.gender == 'F':
            gender_colour_class = config_dict['female_colour_class']
        else:
            gender_colour_class = config_dict['male_colour_class']
        results.append(namedresult(r, guntime, pace, gender_colour_class))
    context = {'results': results,
               'year': year,
               'years': years,
               'gender': gender,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/wins.html', context)
