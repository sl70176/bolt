from django.shortcuts import render
from ..models import Multisport

def index(request):
    all_events = Multisport.objects.values(
                 'date',
                 'event_type',
                 'distance',
                 'race',
                 'resultsurl',
                 'event_slug',
                 ).distinct().order_by('-date')
    seen_event_slugs = []
    events = []
    for i in all_events:
        if i['event_slug'] in seen_event_slugs:
            pass
        else:
            seen_event_slugs.append(i['event_slug'])
            if 'sportstats' in i['resultsurl']:
                i['resultsurl'] = i['resultsurl'].split('&bib')[0]
            events.append(i)
    context = {'events': events}
    return render(request, 'boltapp/multisport_events.html', context)
