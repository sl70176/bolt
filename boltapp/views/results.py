from django.shortcuts import render
from django import db
from django.http import HttpResponse, Http404
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

namedsplit = namedtuple('ns', ['split_num', 'split_time'])
named_std = namedtuple('ns', ['letter', 'colour'])

def index(request, year, race_slug, distance_slug):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    else:
        gender = 'FM'
    showcategory = False
    female_seen = False
    male_seen = False
    split_counter = []
    namedresult = namedtuple('nr', ['athlete',
                                    'colourclass',
                                    'chiptime',
                                    'guntimemicroseconds',
                                    'pace',
                                    'fivek_equiv',
                                    'place',
                                    'gender_place',
                                    'category_place',
                                    'category',
                                    'glyph',
                                    'splits',
                                    'std'])
    try:
        event = Event.objects.get(race__slug=race_slug,
                                  distance__slug=distance_slug,
                                  date__icontains=year)
    except:
        raise Http404('Event not found')
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    std_colour_dict = {'A': config_dict['standard_a_colour_class'],
                       'B': config_dict['standard_b_colour_class'],
                       'C': config_dict['standard_c_colour_class']}
    std_dict = get_std_dict(event)
    distance = Distance.objects.get(slug=distance_slug)
    distance_ids = Result.objects.filter(event__race__slug=race_slug, event__date__icontains=year, athlete__isguest=False).exclude(athlete__quitdate__lte=event.date).values_list('event__distance', flat=True)
    distances = Distance.objects.filter(pk__in=set(distance_ids)).order_by('-km')
    if race_slug == 'endurrun':
        distances = endursort(distances)
    #rawresults = Result.objects.filter(event__race=event.race, event__distance=distance)
    #dates = rawresults.order_by('-event__date').values_list('event__date', flat=True).distinct()
    #years = sorted([ x.year for x in dates ], reverse=True)
    years = get_years(event)
    dbresults = Result.objects.filter(event=event, athlete__isguest=False).order_by('chiptime')
    dist_pbs = dict(Athlete.objects.filter(result__event__distance=distance,
                                   result__event__date__lt=event.date) \
                           .annotate(min_chiptime=Min('result__chiptime')) \
                           .values_list('id', 'min_chiptime'))
    fivek_equiv_pbs = dict(Athlete.objects
                           .filter(result__event__date__lt=event.date) \
                           .annotate(min_fivek_equiv=Min('result__fivek_equiv')) \
                           .values_list('id', 'min_fivek_equiv'))
    exclude_athletes = (Athlete
                      .objects
                      .filter(quitdate__lt=event.date)
                      .values_list('id', flat=True))
    results = []
    for result in dbresults:
    # Skip athletes who quit before this race
        if int(result.athlete.id) in exclude_athletes:
            continue
        if result.athlete.gender == 'F':
            colourclass = female_colour_class
            female_seen = True
        else:
            colourclass = male_colour_class
            male_seen = True
        if result.athlete.gender not in gender:
            continue
        chiptime = (result.chiptime - 
                    timedelta(microseconds=result.chiptime.microseconds))
        pace = get_pace(chiptime, distance.km)
        fivek_equiv = str(result.fivek_equiv).lstrip('0:')
        glyph = False
        if result.athlete.id in dist_pbs:
            if result.chiptime < dist_pbs[result.athlete.id]:
                glyph = 'flash'
        else:
            glyph = 'flash'
        if result.athlete.id in fivek_equiv_pbs:
            if result.fivek_equiv < fivek_equiv_pbs[result.athlete.id]:
                glyph = 'star'
        else:
            glyph = 'star'
        if result.category.name == '':
            category_place = ''
        else:
            category_place = result.category_place
        if result.category.name != '':
            showcategory = True
        splits = get_splits(result)
        if len(splits) > len(split_counter):
            split_counter = []
            for i in range(1, len(splits)+1):
                split_counter.append(i)
        std = False
        if result.athlete in std_dict:
            letter = std_dict[result.athlete]
            colour = std_colour_dict[letter]
            std = named_std(letter, colour)
        guntimemicroseconds = int(result.guntime.total_seconds() * 1000000)
        results.append(namedresult(result.athlete,
                                   colourclass,
                                   chiptime,
                                   guntimemicroseconds,
                                   pace,
                                   fivek_equiv,
                                   result.place,
                                   result.gender_place,
                                   category_place,
                                   result.category.name,
                                   glyph,
                                   splits,
                                   std))
    results = sorted(results, key=attrgetter('place'))
    resultsurl = event.resultsurl
    showgenders = False
    if male_seen and female_seen:
        showgenders = True
    if event.race.series:
        if event.race.series.slug == 'runwaterloo':
            resultsurl = 'http://results.runwaterloo.com/event/{}/{}/{}/'.format(year, race_slug, distance_slug)
    context = {'results': results,
               'event': event,
               'distance_slug': distance_slug,
               'distances': distances,
               'years': years,
               'resultsurl': resultsurl,
               'gender': gender,
               'showcategory': showcategory,
               'showgenders': showgenders,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class,
               'split_counter': split_counter}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/results.html', context)

def get_splits(result):
    ### return a list of splits
    splits = []
    dbsplits = Split.objects.filter(event=result.event, place=result.place).order_by('split_num')
    for s in dbsplits:
        split_time = str(s.split_time).lstrip('0:').split('.')[0]
        splits.append(namedsplit(s.split_num, split_time))
    return splits

def get_years(event):
    years = []
    named_year = namedtuple('ny', ['year', 'slug'])
    races = create_samerace_list(event.race)
    rawresults = Result.objects.filter(event__distance=event.distance, event__race__in=races)
    dates = rawresults.order_by('-event__date').values_list('event__date', 'event__race__slug').distinct()
    for d in dates:
        years.append(named_year(d[0].year, d[1]))
    return years

def get_std_dict(event):
    std_dict = {}
    dbstandards = Standardcalc.objects.filter(event=event)
    for s in dbstandards:
        std_dict[s.athlete] = s.standarddef.letter
    return std_dict
