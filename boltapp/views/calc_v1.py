from django.shortcuts import render
from django.http import HttpResponse
from collections import namedtuple
from datetime import date, timedelta
from operator import attrgetter
import urllib
from ..models import *
from ..utils import *

named_dropdown_filter = namedtuple('nyf', ['current_value', 'btn_class', 'items'])
named_item = namedtuple('ni', ['value', 'url'])

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    asofdate = date.today()
    message = 'Coming soon!'
    athletes = Athlete.objects.all()
    distances = Distance.objects.all().order_by('km')
    wrdict = get_wrdict(asofdate, 'FM', 'any-age')
    wrlist = wrdict2list(wrdict, False, False)
    d2e = int(Config.objects.get(name='d2e').value)
    startdate = (asofdate - timedelta(days=(d2e - 1)))
    wr_sorted_athletes = sorted(wrlist, key=attrgetter('athlete_name'))
    wr_higher_athletes = []
    athlete1name = athlete2name = ''
    athlete1best = athlete1secondbest = athlete1thirdbest = 0
    athlete2best = athlete2secondbest = athlete2thirdbest = 0
    athlete1avgaseconds = athlete2avgaseconds = 0
    if 'hunter' in qstring:
        hunter = qstring['hunter'][0]
        hunter = Athlete.objects.get(slug=hunter)
        if hunter.quitdate:
            athlete1_id = False
        else:
            athlete1_id = int(hunter.id)
    else:
        athlete1_id = 0
        hunter = False
    if 'hunted' in qstring:
        hunted = qstring['hunted'][0]
        hunted = Athlete.objects.get(slug=hunted)
        athlete2_id = int(hunted.id)
    else:
        athlete2_id = 0
        hunted = False
    if athlete1_id > 0:
        athlete1name = hunter.name
        athlete1results = Result.objects.filter(athlete=hunter,
                                        event__date__gte=startdate,
                                        event__date__lte=asofdate).order_by('fivek_equiv')
        athlete1best = athlete1results[0].fivek_equiv.total_seconds()
        athlete1secondbest = athlete1results[1].fivek_equiv.total_seconds()
        athlete1thirdbest = athlete1results[2].fivek_equiv.total_seconds()
    if athlete2_id > 0:
        athlete2name = hunted.name
        athlete2results = Result.objects.filter(athlete=hunted,
                                         event__date__gte=startdate,
                                         event__date__lte=asofdate).order_by('fivek_equiv')
        athlete2best = athlete2results[0].fivek_equiv.total_seconds()
        athlete2secondbest = athlete2results[1].fivek_equiv.total_seconds()
        athlete2thirdbest = athlete2results[2].fivek_equiv.total_seconds()
    for athlete in wrlist:
        if athlete.athlete.id != athlete1_id:
            wr_higher_athletes.insert(0, athlete)
            if athlete.athlete.id == athlete2_id:
                athlete2avgaseconds = athlete.avgaseconds
        else:
            athlete1avgaseconds = athlete.avgaseconds
            break
    if athlete1_id > 0 and athlete2_id == 0:
        secondstopass = float(athlete1avgaseconds - athlete1avgaseconds) + (1/3)
    else:
        seconds2tie = athlete1avgaseconds - athlete2avgaseconds
        sectoruntie = athlete1thirdbest - (3 * seconds2tie)
        if sectoruntie % 1 > 0.9999999:
            sectoruntie = float(int(sectoruntie) + 1)
        athlete1newresults = (int(sectoruntie), int(athlete1best),
                              int(athlete1secondbest))
        athlete1newresults = sorted(athlete1newresults)
        athlete2results = [int(athlete2best), int(athlete2secondbest),
                           int(athlete2thirdbest)]
        if athlete1newresults < athlete2results:
            secondstopass = seconds2tie
        elif athlete1newresults == athlete2results:
            if athlete1name < athlete2name:
                secondstopass = seconds2tie
            else:
                secondstopass = seconds2tie + (1/3)
        else:
            secondstopass = seconds2tie + (1/3)
    if secondstopass % 1 < 0.000001:
         secondstopass = int(secondstopass)
    sectorun = float(athlete1thirdbest) - (3 * secondstopass)
    if sectorun % 1 > 0.9999999:
        sectorun = int(sectorun) + 1
    passpossible = True
    if sectorun < 757:
        passpossible = False
    timetorun = truncate_time(timedelta(seconds=sectorun))
    namedtimes = namedtuple('nt', ['distname', 'time'])
    timestorun = []
    for distance in distances:
        if distance.slug == '21_34-km':
            continue
        thissec = int(float(sectorun) / float(distance.c25k))
        # check if running 1 second higher would also be ok
        while True:
            if round(thissec * distance.c25k) <= sectorun:
                thissec += 1
            else:   # oops, went too far
                thissec -= 1
                break
        thistime = str(truncate_time(timedelta(seconds=thissec))).lstrip('0:')
        timestorun.append(namedtimes(distance.name, thistime))


    context = {'wr_sorted_athletes': wr_sorted_athletes,
               'wr_higher_athletes': wr_higher_athletes,
               'hunter': hunter,
               'hunted': hunted,
               'timestorun': timestorun,
               'passpossible': passpossible}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/calc_v1.html', context)
