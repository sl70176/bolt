from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    year = False
    if 'year' in qstring:
        year = int(qstring['year'][0])
    results = []
    iyears = Endurathletebattle.objects.values_list('battle_year', flat=True)
    tyears = Endurteambattle.objects.values_list('battle_year', flat=True)
    syears = Endurteamsuperbattle.objects.values_list('battle_year', flat=True)
    years = list(iyears) + list(tyears) + list(syears)
    years = sorted(set(years), reverse=True)
    athlete_battles = Endurathletebattle.objects.all().order_by('-battle_year', 'sortorder')
    if year:
        athlete_battles = athlete_battles.filter(battle_year=year)
    team_battles = Endurteambattle.objects.all().order_by('-battle_year', 'sortorder')
    if year:
        team_battles = team_battles.filter(battle_year=year)
    super_battles = Endurteamsuperbattle.objects.all().order_by('-battle_year', 'sortorder')
    if year:
        super_battles = super_battles.filter(battle_year=year)
    context = {'athlete_battles': athlete_battles,
               'team_battles': team_battles,
               'super_battles': super_battles,
               'year': year,
               'years': years}
    return render(request, 'boltapp/endurrun_battles.html', context)
