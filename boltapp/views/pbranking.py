from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

named_dropdown_filter = namedtuple('nyf', ['current_value', 'btn_class', 'items'])
named_item = namedtuple('ni', ['value', 'url'])

def index(request, distance_slug):
    """ Create a list of values for this record """
    gender = 'FM'
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    qsage = 'any-age'
    if 'age' in qstring:
        qsage = qstring['age'][0]
        minage, maxage = Agefilter.objects.filter(slug=qsage).values_list('minage', 'maxage')[0]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    distance = Distance.objects.get(slug=distance_slug)
    distances = Distance.objects.filter(ispb=True).order_by('km')
    named_pb = namedtuple('npb', ['result', 'seconds', 'pbtime', 'pace', 'colour_class'])
    results = []
    athletes = Athlete.objects.filter(isguest=False)
    if gender != 'FM':
        athletes = athletes.filter(gender=gender)
    for athlete in athletes:
        dbresults = Result.objects.filter(athlete=athlete, event__distance__slug=distance_slug)
        dbresults = dbresults.filter(event__date__gte=athlete.joindate)
        # Exclude former members
        if athlete.quitdate:
            #dbresults = dbresults.filter(event__date__lte=athlete.quitdate)
            continue
        if qsage == 'any-age':
            pb = dbresults.order_by('chiptime')[:1]
        else:
            pb = []
            for r in dbresults.order_by('chiptime'):
               athlete_age = get_age(r.event.date, athlete.dob)
               if minage <= athlete_age <= maxage:
                   pb.append(r)
                   break
        if len(pb) == 1:
            seconds = int(pb[0].chiptime.total_seconds())
            pbtime = truncate_time(pb[0].chiptime)
            pace = get_pace(pb[0].chiptime, distance.km)
            if pb[0].athlete.gender == 'F':
                colour_class = female_colour_class
            else:
                colour_class = male_colour_class
            results.append(named_pb(pb[0], seconds, pbtime, pace, colour_class))
    age_filter = make_age_filter(qsage, distance_slug, gender)
    results = sorted(results, key=attrgetter('seconds'))
    context = {'results': results,
               'distance': distance,
               'distances': distances,
               'gender': gender,
               'qsage': qsage,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class,
               'age_filter': age_filter}
    return render(request, 'boltapp/pbranking.html', context)

def make_age_filter(qsage, distance_slug, qsgender):
    btn_class = 'default'
    qs_params = []
    items = []
    if qsgender != 'FM':
        qs_params.append('gender={}'.format(qsgender))
    for af in Agefilter.objects.all().order_by('sortorder'):
        if af.slug == 'any-age':
            age_qs = '/?' + '&'.join(sorted(qs_params))
            if age_qs == '/?':
                age_qs = ''
            if af.slug != qsage:
                items.append(named_item(af.name, '/pbranking/{}{}'.format(distance_slug, age_qs)))
        else:
            age_params =  ['age={}'.format(af.slug),] + qs_params
            age_qs = '/?' + '&'.join(sorted(age_params))
            if af.slug != qsage:
                items.append(named_item(af.name, '/pbranking/{}{}'.format(distance_slug, age_qs)))
    current_value = Agefilter.objects.get(slug=qsage).name
    age_filter = named_dropdown_filter(current_value, btn_class, items)
    return age_filter
