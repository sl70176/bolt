from django.shortcuts import render
from ..models import *
from ..utils import *

def index(request):
    """ Create a list of recent career bests """
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    results =  get_recent_pbs(50)
    context = {'results': results,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/career_bests.html', context)
