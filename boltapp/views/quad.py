from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    """ Create a list of values for this record """
    gender = 'FM'
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    results =  get_results(gender, female_colour_class, male_colour_class)
    context = {'results': results,
               'gender': gender,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/quad.html', context)

def get_results(gender, female_colour_class, male_colour_class):
    named_quad = namedtuple('nq', ['athlete',
                                   'colour_class',
                                   'fivek_equiv',
                                   'fivek_equivstr',
                                   'fivek',
                                   'fivekstr',
                                   'tenk',
                                   'tenkstr',
                                   'tenk_fivek_equiv',
                                   'half',
                                   'halfstr',
                                   'half_fivek_equiv',
                                   'full',
                                   'fullstr',
                                   'full_fivek_equiv'])

    results = []
    athletes = Athlete.objects.filter(isguest=False)
    if gender != 'FM':
        athletes = athletes.filter(gender=gender)
    for athlete in athletes:
        total_seconds = 0
        if athlete.quitdate:    # skip quitters
            continue
        if athlete.gender == 'F':
            colour_class = female_colour_class
        else:
            colour_class = male_colour_class
        try:
            fivek = Result.objects.filter(athlete=athlete, event__distance__slug='5-km').order_by('chiptime')[0]
        except:
            continue
        else:
            fivekstr = str(fivek.chiptime).split('.')[0].lstrip('0:')
            total_seconds += fivek.fivek_equiv.seconds
        try:
            tenk = Result.objects.filter(athlete=athlete, event__distance__slug='10-km').order_by('chiptime')[0]
        except:
            continue
        else:
            tenkstr = str(tenk.chiptime).split('.')[0]
            tenk_fivek_equiv = str(tenk.fivek_equiv).lstrip('0:')
            total_seconds += tenk.fivek_equiv.seconds
        try:
            half = Result.objects.filter(athlete=athlete, event__distance__slug='half-marathon').order_by('chiptime')[0]
        except:
            continue
        else:
            halfstr = str(half.chiptime).split('.')[0]
            half_fivek_equiv = str(half.fivek_equiv).lstrip('0:')
            total_seconds += half.fivek_equiv.seconds
        try:
            full = Result.objects.filter(athlete=athlete, event__distance__slug='marathon').order_by('chiptime')[0]
        except:
            continue
        else:
            fullstr = str(full.chiptime).split('.')[0]
            full_fivek_equiv = str(full.fivek_equiv).lstrip('0:')
            total_seconds += full.fivek_equiv.seconds
        fivek_equiv = total_seconds / 4
        fivek_equivstr = str(timedelta(seconds=round(fivek_equiv))).lstrip('0:')
        results.append(named_quad(athlete, colour_class, fivek_equiv, fivek_equivstr, fivek, fivekstr, tenk, tenkstr, tenk_fivek_equiv, half, halfstr, half_fivek_equiv, full, fullstr, full_fivek_equiv))
    results = sorted(results, key=attrgetter('fivek_equiv'))
    return results
