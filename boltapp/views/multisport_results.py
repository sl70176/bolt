from django.shortcuts import render
import urllib
from ..models import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    results = Multisport.objects.all().order_by('-date', 'race', 'place')
    resultsurl = None
    show_bike_types = False
    dist_for_speed = False
    if 'event' in qstring:
        event_slug = qstring['event'][0]
        results = results.filter(event_slug=event_slug)
        resultsurl = results[0].resultsurl
        if 'sportstats' in resultsurl:
            resultsurl = results[0].resultsurl.split('&bib')[0]
        if results.exclude(bike_type='').count() > 0:
            show_bike_types = True
        if 'hp' in event_slug and 'time-trial-series' in event_slug:
            dist_for_speed = 12.2
    if 'athlete' in qstring:
        athlete_slug = qstring['athlete'][0]
        results = results.filter(athlete__slug=athlete_slug)
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']                     
    male_colour_class = config_dict['male_colour_class']     
    context = {'qstring': qstring,
               'results': results,
               'resultsurl': resultsurl,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class,
               'show_bike_types' : show_bike_types,
               'dist_for_speed' : dist_for_speed, 
               }
    return render(request, 'boltapp/multisport_results.html', context)
