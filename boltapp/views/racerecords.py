from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, race_slug, distance_slug):
    """ Create a list of values for this race """
    gender = 'FM'
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    race = Race.objects.get(slug=race_slug)
    races = create_samerace_list(race)
    distance = Distance.objects.get(slug=distance_slug)
    distance_ids = Result.objects.filter(event__race__slug=race_slug).values_list('event__distance', flat=True)
    distances = Distance.objects.filter(pk__in=set(distance_ids)).order_by('-km')
    if race_slug == 'endurrun':
        distances = endursort(distances)
    named_pb = namedtuple('npb', ['result', 'seconds', 'pbtime', 'pace', 'colour_class'])
    results = []
    athletes = Athlete.objects.filter(isguest=False)
    if gender != 'FM':
        athletes = athletes.filter(gender=gender)
    for athlete in athletes:
        dbresults = Result.objects.filter(athlete=athlete,
                                          event__distance__slug=distance_slug,
                                          event__race__in=races)
        dbresults = dbresults.filter(event__date__gte=athlete.joindate)
        if athlete.quitdate:
            dbresults = dbresults.filter(event__date__lte=athlete.quitdate)
        pb = dbresults.order_by('chiptime')[:1]
        if len(pb) == 1:
            seconds = int(pb[0].chiptime.total_seconds())
            pbtime = truncate_time(pb[0].chiptime)
            pace = get_pace(pb[0].chiptime, distance.km)
            if pb[0].athlete.gender == 'F':
                colour_class = female_colour_class
            else:
                colour_class = male_colour_class
            results.append(named_pb(pb[0], seconds, pbtime, pace, colour_class))
    results = sorted(results, key=attrgetter('seconds'))
    context = {'results': results,
               'race': race,
               'distance': distance,
               'distances': distances,
               'gender': gender,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/racerecords.html', context)
