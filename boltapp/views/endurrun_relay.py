from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min, Sum
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'year' in qstring:
        year = int(qstring['year'][0])
    else:
        year = False 
    namedresult = namedtuple('nr', ['endurteam',
                                    'year',
                                    'stages',
                                    'total_time',
                                    'pace',
                                    'placegap',
                                    'leadgap'])
    config_dict = dict(Config.objects.values_list('name', 'value'))
    endurrun_first_year = int(config_dict['endurrun_first_year'])
    this_year = date.today().year
    years = []
    loop_year = this_year
    while True:
        if loop_year < 2012:
            break
        years.append(loop_year)
        loop_year -= 1
    dbresults = Result.objects.filter(event__race__name__icontains='endurrun')
    results = []
    loop_year = this_year
    endurstages = Config.objects.filter(name__icontains='endurstage')
    stage_distances = {}
    for stage in range(1, 8):
        distance_slug = endurstages.get(value=stage).name.split('_', 1)[1]
        distance = Distance.objects.get(slug=distance_slug)
        stage_distances[stage] = distance
    while loop_year >= endurrun_first_year:
        if year:
            if loop_year != year:
                loop_year -= 1
                continue
        yearresults = dbresults.filter(event__date__icontains=loop_year)
        endurteams = Endurteam.objects.filter(year=loop_year)
        for endurteam in endurteams:
            total_time = timedelta(seconds=0)
            total_distance = 0
            total_stages = 0
            for stage in range(1, 8):
                ath_expr = 'endurteam.st{}'.format(stage)
                athlete = eval(ath_expr)
                try: 
                    result = yearresults.get(event__distance=stage_distances[stage], athlete=athlete)
                except:
                    result = 'blah'
                else:
                    total_time += result.guntime
                    total_distance += result.event.distance.km
                    total_stages += 1
            pace = get_pace(total_time, total_distance)
            results.append(namedresult(endurteam, loop_year, total_stages,
                                        total_time, pace, 0, 0))
        loop_year -= 1
    results = sorted(results, key=attrgetter('year'))
    results = sorted(results, key=attrgetter('total_time'))
    results = sorted(results, key=attrgetter('stages'), reverse=True)
    gapresults = []
    # Calculate gaps
    for i, r in enumerate(results):
        if i == 0:
            stages_required = r.stages
            previous_time = lead_time = r.total_time
            placegap = '-'
            leadgap = '-'
        else:
            if r.stages == stages_required:
                if r.total_time == previous_time:
                    placegap = '-'
                else:
                    placegap = r.total_time - previous_time
                previous_time = r.total_time
                if r.total_time == lead_time:
                    leadgap = '-'
                else:
                    leadgap = r.total_time - lead_time
            else:
                placegap = '-'
                leadgap = '-'
        gapresults.append(namedresult(r.endurteam, r.year, r.stages, r.total_time, r.pace, placegap, leadgap))
    results = gapresults
    context = {'results': results,
               'year': year,
               'years': years}
    return render(request, 'boltapp/endurrun_relay.html', context)
