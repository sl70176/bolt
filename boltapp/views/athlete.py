from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, athlete_slug):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    racefilter = 'Current'
    distance = False
    if 'racefilter' in qstring:
        racefilter = qstring['racefilter'][0]
    if 'distance' in qstring:
        distance_slug = qstring['distance'][0]
        distance = Distance.objects.get(slug=distance_slug)
    namedresult = namedtuple('nr', ['event',
                                    'chiptime',
                                    'pace',
                                    'c25k',
                                    'c25kstr',
                                    'place',
                                    'gender_place',
                                    'category_place',
                                    'category',
                                    'resultsurl',
                                    'd2e'])
    athlete = Athlete.objects.get(slug=athlete_slug, isguest=False)
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    std_colour_dict = {'A': config_dict['standard_a_colour_class'],              
                       'B': config_dict['standard_b_colour_class'],              
                       'C': config_dict['standard_c_colour_class']}  
    if athlete.gender == 'F':
        gender_colour_class = config_dict['female_colour_class']
    else:
        gender_colour_class = config_dict['male_colour_class']
    distance_ids = (Result.objects
                    .filter(athlete__slug=athlete_slug).exclude(event__distance__slug='21_34-km')
                    .values_list('event__distance', flat=True)
                   )
    distances = Distance.objects.filter(pk__in=set(distance_ids)).order_by('km')
    dbresults = Result.objects.filter(athlete_id=athlete.id).order_by('-event__date')
    if athlete.quitdate:
        dbresults = dbresults.filter(event__date__lte=athlete.quitdate)
    wincount = dbresults.filter(gender_place=1).count()
    results = []
    for result in dbresults:
        if racefilter == 'Wins':
            if result.gender_place != 1:
                continue
        event = result.event
        chiptime = (result.chiptime - 
                    timedelta(microseconds=result.chiptime.microseconds))
        pace = get_pace(chiptime, event.distance.km)
        c25k = get_5k_equiv(chiptime, event.distance.c25k)
        c25kstr = str(c25k).lstrip('0:')
        resultsurl = event.resultsurl
        if event.race.series:
            if event.race.series.slug == 'runwaterloo':
                resultsurl = ('http://results.runwaterloo.com/event/{}/{}/{}/'
                              .format(event.date.year, event.race.slug, event.distance.slug)
                             )
        if result.gender_place is not None:
            gender_place = result.gender_place
        else:
            gender_place = '' 
        if result.category_place is not None:
            category_place = result.category_place
        else:
            category_place = '' 
        d2e = int(config_dict['d2e']) - ((date.today() - event.date)).days
        if d2e < 1:
            d2e = False
        results.append(namedresult(event,
                                   chiptime,
                                   pace,
                                   c25k,
                                   c25kstr,
                                   result.place,
                                   gender_place,
                                   category_place,
                                   result.category.name,
                                   resultsurl,
                                   d2e))
    results = sorted(results, key=attrgetter('c25k'))
    current_results = [ x for x in results if x.d2e ]
    top3 = [ x.event for x in current_results ][0:3]
    if len(top3) < 3:
        top3 = []
    if racefilter == 'Current':
        results = current_results
        current_distances = set([ x.event.distance for x in current_results ])
        distances = [ x for x in distances if x in current_distances ]
    elif racefilter == 'PBs':
        results = sorted(results, key=attrgetter('chiptime', 'event.date'))
        newresults = []
        distances_seen = []
        for result in results:
            if result.event.distance.slug == '21_34-km':
                continue
            if result.event.distance not in distances_seen:
                newresults.append(result)
                distances_seen.append(result.event.distance)
        results = sorted(newresults, key=attrgetter('c25k'))
    if distance:
        distance_results = [ x for x in results if x.event.distance == distance ]
        results = distance_results
    std_dict =  get_stddict(date.today())
    rawstds = get_stds(std_dict, athlete, std_colour_dict)
    splitstds = []
    if rawstds:
        splitstds = rawstds.mouseover.split('\n')
    cleanstds = []
    named_clean_std = namedtuple('nc', ['letter', 'colour', 'distance'])
    for i in splitstds:
        letter = i[0]
        colour = std_colour_dict[letter]
        distance = i.split(':')[1].strip()
        cleanstds.append(named_clean_std(letter, colour, distance))
    context = {'results': results,
               'athlete': athlete,
               'distances': distances,
               'racefilter': racefilter,
               'distance': distance,
               'top3': top3,
               'gender_colour_class': gender_colour_class,
               'wincount': wincount,
               'rawstds' : rawstds,
               'cleanstds': cleanstds}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/athlete.html', context)
