from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.db.models import Min, Count, Q
from django import db
from collections import namedtuple
from datetime import timedelta
from itertools import chain
import datetime
import urllib
import re

from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    message = ''
    if 'q' not in qstring:
        raise Http404('No search string provided')
    q = qstring['q'][0]
    name_query = get_query(q, ['name',])
    athletes = Athlete.objects.filter(name_query).filter(isguest=False).order_by('name')
    race_query = get_query(q, ['name',])
    races = Race.objects.filter(name_query).order_by('name')

    # Same race workaround for Oktoberfest, need a cleaner way
    # to handle this with database
    oktoberfest = Race.objects.filter(slug='oktoberfest-run')
    fallclassic = Race.objects.filter(slug='fall-classic')
    if oktoberfest[0] in races:
        races = list(chain(races, fallclassic))
    if fallclassic[0] in races:
        races = list(chain(races, oktoberfest))
    # end same race workaround

    past_races = get_races('Past')
    past_races = [ x for x in past_races if x.events[0].race in races ]
    future_races = get_races('Future')
    future_races = [ x for x in future_races if x.events[0].race in races ]
    total_results = len(athletes) + len(past_races) + len(future_races)
    if total_results > 100:
        results = False
        message = 'Found too many results! You\'re going to have to be a little more specific.'
    elif total_results == 0:
        results = False
        message = 'Your search came up empty. That is so sad.'
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    if len(athletes) == 1 and total_results == 1:
        redirecturl = '/athlete/{}/'.format(athletes[0].slug)
        context = {'redirecturl': redirecturl}
        return render(request, 'boltapp/search_redirect.html', context)
    else:
        context = {'q': q,
                   'message': message,
                   'athletes': athletes,
                   'past_races': past_races,
                   'future_races': future_races,
                   'female_colour_class': female_colour_class,
                   'male_colour_class': male_colour_class}
    #print(len(db.connection.queries))   # number of sql queries that happened
        return render(request, 'boltapp/search.html', context)

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:

        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']

    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.

    '''
    query = None # Query to search for every search term        
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query
