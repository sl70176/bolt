from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min, Sum
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    else:
        gender = 'FM'
    if 'year' in qstring:
        year = int(qstring['year'][0])
    else:
        year = False 
    female_seen = False
    male_seen = False
    namedresult = namedtuple('nr', ['athlete',
                                    'colourclass',
                                    'year',
                                    'stages',
                                    'total_time',
                                    'pace',])
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    endurrun_first_year = int(config_dict['endurrun_first_year'])
    this_year = date.today().year
    years = []
    loop_year = this_year
    while True:
        if loop_year < 2012:
            break
        years.append(loop_year)
        loop_year -= 1
    dbresults = Result.objects.filter(event__race__name__icontains='endurrun',
                                      athlete__isguest=False)
    results = []
    loop_year = this_year
    while loop_year >= endurrun_first_year:
        if year:
            if loop_year != year:
                loop_year -= 1
                continue
        try:
            known_ultimates = eval(Config.objects.get(name='endurultimates_{}'.format(loop_year)).value)
        except:
            known_ultimates = []
        max_quit_date = date(loop_year, 8, 1)
        athlete_ids = set(dbresults.filter(event__date__icontains=loop_year).values_list('athlete', flat=True))
        athlete_ids = set(list(athlete_ids) + known_ultimates)
        for a in athlete_ids:
            athlete = Athlete.objects.get(id=a)
            if athlete.quitdate:
                if athlete.quitdate < max_quit_date:
                    continue
            athlete_results = dbresults.filter(event__date__icontains=loop_year, athlete=athlete).order_by('event__date')
            stages = athlete_results.count()
            if stages < 7:
                if not year:
                    continue
                if a not in known_ultimates:
                    continue
            total_time = athlete_results.aggregate(Sum('guntime'))['guntime__sum']
            if not total_time:
                total_time = timedelta(seconds=0)
            total_distance = athlete_results.aggregate(Sum('event__distance__km'))['event__distance__km__sum']
            if total_distance:
                pace = get_pace(total_time, total_distance)
            else:
                pace = ''
            if athlete.gender == 'F':
                colourclass = female_colour_class
                female_seen = True
            else:
                colourclass = male_colour_class
                male_seen = True
            if athlete.gender not in gender:
                continue
            results.append(namedresult(athlete,
                                       colourclass,
                                       loop_year,
                                       stages,
                                       total_time,
                                       pace))
        loop_year -= 1
    showgenders = False
    if male_seen and female_seen:
        showgenders = True
    results = sorted(results, key=attrgetter('athlete.name'))
    results = sorted(results, key=attrgetter('total_time'))
    results = sorted(results, key=attrgetter('stages'), reverse=True)
    context = {'results': results,
               'year': year,
               'years': years,
               'gender': gender,
               'showgenders': showgenders,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/endurrun_ultimate.html', context)
