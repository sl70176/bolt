from django.shortcuts import render
from django.http import HttpResponse, Http404
from collections import namedtuple
from datetime import date, timedelta
from operator import attrgetter
import urllib
from ..models import *
from ..utils import *

named_dropdown_filter = namedtuple('nyf', ['current_value', 'btn_class', 'items'])
named_item = namedtuple('ni', ['value', 'url'])

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    asofdate = date.today()
    athletes = Athlete.objects.all()
    distances = Distance.objects.all().order_by('km')
    wrdict = get_wrdict(asofdate, 'FM', 'any-age')
    wrlist = wrdict2list(wrdict, False, False)
    d2e = int(Config.objects.get(name='d2e').value)
    startdate = (asofdate - timedelta(days=(d2e - 1)))
    wr_sorted_athletes = sorted(wrlist, key=attrgetter('athlete_name'))
    wr_higher_athletes = []
    athlete1name = athlete2name = ''
    athlete1best = athlete1secondbest = athlete1thirdbest = 0
    athlete2best = athlete2secondbest = athlete2thirdbest = 0
    athlete1avgaseconds = athlete2avgaseconds = 0
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    if 'athlete' in qstring:
        athlete_slug = qstring['athlete'][0]
        athlete = Athlete.objects.get(slug=athlete_slug)
        if athlete.quitdate:
            athlete1_id = False
        else:
            athlete1_id = int(athlete.id)
    else:
        athlete1_id = 0
        athlete = False
    if 'distance' in qstring:
        distance_slug = qstring['distance'][0]
        distance = Distance.objects.get(slug=distance_slug)
    else:
        distance = False
    if athlete:
        athlete1name = athlete.name
        athlete1results = Result.objects.filter(athlete=athlete,
                                        event__date__gte=startdate,
                                        event__date__lte=asofdate).order_by('fivek_equiv')
        if len(athlete1results) < 3:
            raise Http404('Calc only works for athletes with 3 or more current results')
        athlete1best = athlete1results[0].fivek_equiv.total_seconds()
        athlete1secondbest = athlete1results[1].fivek_equiv.total_seconds()
        athlete1thirdbest = athlete1results[2].fivek_equiv.total_seconds()
    for a in wrlist:
        wr_higher_athletes.insert(0, a)
        if a.athlete.id == athlete1_id:
            athlete1avgaseconds = a.avgaseconds
            break
    results = []
    namedresult = namedtuple('nt', ['athlete', 'time', 'pace', 'colour_class'])
    for a in wr_higher_athletes:
        if not distance:
            continue
        athlete2name = a.athlete.name
        athlete2avgaseconds = a.avgaseconds
        athlete2results = Result.objects.filter(athlete=a.athlete,
                                         event__date__gte=startdate,
                                         event__date__lte=asofdate).order_by('fivek_equiv')
        athlete2best = athlete2results[0].fivek_equiv.total_seconds()
        athlete2secondbest = athlete2results[1].fivek_equiv.total_seconds()
        athlete2thirdbest = athlete2results[2].fivek_equiv.total_seconds()
        seconds2tie = athlete1avgaseconds - athlete2avgaseconds
        sectoruntie = athlete1thirdbest - (3 * seconds2tie)
        if sectoruntie % 1 > 0.9999999:
            sectoruntie = float(int(sectoruntie) + 1)
        athlete1newresults = (int(sectoruntie), int(athlete1best),
                              int(athlete1secondbest))
        athlete1newresults = sorted(athlete1newresults)
        athlete2results = [int(athlete2best), int(athlete2secondbest),
                           int(athlete2thirdbest)]
        if athlete1newresults < athlete2results:
            secondstopass = seconds2tie
        elif athlete1newresults == athlete2results:
            if athlete1name < athlete2name:
                secondstopass = seconds2tie
            else:
                secondstopass = seconds2tie + (1/3)
        else:
            secondstopass = seconds2tie + (1/3)
        if secondstopass % 1 < 0.000001:
           secondstopass = int(secondstopass)
        sectorun = float(athlete1thirdbest) - (3 * secondstopass)
        if sectorun % 1 > 0.9999999:
            sectorun = int(sectorun) + 1
        passpossible = True
        if sectorun < 757:
            continue
        timetorun = truncate_time(timedelta(seconds=sectorun))
        thissec = int(float(sectorun) / float(distance.c25k))
        # check if running 1 second higher would also be ok
        while True:
            if round(thissec * distance.c25k) <= sectorun:
                thissec += 1
            else:   # oops, went too far
                thissec -= 1
                break

        pace = get_pace(timedelta(seconds=thissec), distance.km)
        thistime = str(truncate_time(timedelta(seconds=thissec))).lstrip('0:')
        if a.athlete.gender == 'F':
            colour_class = female_colour_class
        else:
            colour_class = male_colour_class
        results.append(namedresult(a.athlete, thistime, pace, colour_class))
    context = {'wr_sorted_athletes': wr_sorted_athletes,
               'wr_higher_athletes': wr_higher_athletes,
               'athlete': athlete,
               'distance': distance,
               'distances': distances,
               'results': results}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/calc.html', context)
