from django.http import HttpResponse
from ..models import Indexcalc

def index(request):
    """ Grab a cached version of the index view """
    html = Indexcalc.objects.get(id=1).value
    response = HttpResponse(html)
    return response
