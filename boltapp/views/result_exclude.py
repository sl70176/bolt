from django.shortcuts import render, redirect
#from django import db
#from django.http import HttpResponse, Http404
#from django.db.models import Min
#from collections import namedtuple
#from operator import attrgetter
from datetime import timedelta
import urllib
from ..models import *

def index(request, event_id, athlete_id, guntime):
    if not request.user.is_authenticated:
        return redirect('/admin/login/?next={}'.format(request.META['REQUEST_URI']))
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    event = Event.objects.get(id=event_id)
    guntime_td = timedelta(microseconds=int(guntime))
    result = Result.objects.get(event_id=event.id,
                                athlete_id=athlete_id,
                                guntime=(guntime_td))
    if 'confirmed' in qstring:
        # add entry to database
        Resultexclude(event=event,
                      athlete_id=athlete_id,
                      guntime=guntime_td).save()
        result.delete()
        year = event.date.year
        race_slug = event.race.slug
        distance_slug = event.distance.slug
        return redirect('/results/{}/{}/{}/'.format(year, race_slug, distance_slug))
    else:
        context = {'result': result}
        return render(request, 'boltapp/result_exclude.html', context)

