from django.shortcuts import render
import urllib
from ..models import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    redirecturl = False

    # replace old style athlete query parameter
    if 'athlete' in qstring:
        qstring['athlete_id'] = qstring.pop('athlete')

    page = qstring['page'][0]

    if page == 'about':
        redirecturl = '/about'

    if page == 'athlete':
        if 'athlete_id' in qstring:
            athlete_id = qstring['athlete_id'][0]
            athlete_slug = Athlete.objects.get(id=athlete_id).slug
            redirecturl = '/athlete/{}'.format(athlete_slug)
        else:
            redirecturl = '/'

    if page == 'calc':
        redirecturl = '/calc'

    if page == 'endurbattles':
        redirecturl = '/endurrun/battles'

    if page == 'enduribattle':
        qs_athlete1 = qstring['athlete1'][0]
        athlete1 = Athlete.objects.get(id=qs_athlete1)
        year1 = qstring['team'][0]
        qs_athlete2 = qstring['athlete2'][0]
        athlete2 = Athlete.objects.get(id=qs_athlete2)
        year2 = qstring['team2'][0]
        redirecturl = '/endurrun/battle/athlete/{}/{}/{}/{}/'.format(year1, athlete1.slug, year2, athlete2.slug)

    if page == 'endurbattle':
        qs_team1, qs_team2 = qstring['endurteam_id']
        team1 = Endurteam.objects.get(id=qs_team1)
        team2 = Endurteam.objects.get(id=qs_team2)
        redirecturl = '/endurrun/battle/team/{}/{}/{}/{}/'.format(team1.year, team1.slug, team2.year, team2.slug)

    if page == 'races':
        if 'raceid' in qstring:
            event_id = qstring['raceid'][0]
            event = Event.objects.get(id=event_id)
            redirecturl = '/results/{}/{}/{}'.format(event.date.year,
                                                     event.race.slug,
                                                     event.distance.slug)
        else:
            redirecturl = '/races'

    if page == 'records':
        if 'record' in qstring:
            record = qstring['record'][0]
            if record == '3':
                redirecturl = '/pbranking/5-km'

    if page == 'strava':
        if 'stravaid' not in qstring:
            qstring['stravaid'] = '1'
        stravaid = qstring['stravaid'][0]
        if stravaid == '1':
            redirecturl = '/strava/segments/map'
        if stravaid == '3':
            redirecturl = '/strava/crs/top-40'

    context = {'redirecturl': redirecturl}
    return render(request, 'boltapp/redirect.html', context)
