from django.shortcuts import render
from datetime import date, timedelta
from ..models import (
    Athlete,
    Config,
    Distance,
    Endurteamsuperbattle,
    Event,
    Result,
    )
from ..utils import get_wrdict


def index(request, year, team1_slug, team2_slug):
    battle = Endurteamsuperbattle.objects.get(
        battle_year=year,
        team1_slug=team1_slug,
        team2_slug=team2_slug,
        )
    team1_name = battle.team1_name
    team2_name = battle.team2_name
    teams1 = battle.teams1
    teams2 = battle.teams2
    config_dict = dict(Config.objects.values_list('name', 'value'))
    endurbattle_colour_class1 = config_dict['endurbattle_colour_class1']
    endurbattle_colour_class2 = config_dict['endurbattle_colour_class2']
    endurstages = Config.objects.filter(name__icontains='endurstage')
    year_date = date(int(year), 7, 31)
    year_ranking = get_wrdict(year_date, 'FM', 'any-age')
    results = []
    for stage in range(1, 8):
        distance_slug = endurstages.get(value=stage).name.split('_', 1)[1]
        distance = Distance.objects.get(slug=distance_slug)
        event = Event.objects.get(
            date__icontains=year,
            distance=distance,
            race__name__icontains='endurrun',
            )
        stage_results = dict(Result.objects.filter(event=event)
                             .values_list('athlete', 'chiptime'))
        athletes1 = []
        for i in teams1.values():
            athlete_id = i['st{}_id'.format(stage)]
            athlete = Athlete.objects.get(id=athlete_id)
            vathlete = VAthlete(athlete, year_ranking, stage_results)
            athletes1.append(vathlete)
        athletes2 = []
        for i in teams2.values():
            athlete_id = i['st{}_id'.format(stage)]
            athlete = Athlete.objects.get(id=athlete_id)
            vathlete = VAthlete(athlete, year_ranking, stage_results)
            athletes2.append(vathlete)
        distance_slug = endurstages.get(value=stage).name.split('_', 1)[1]
        distance = Distance.objects.get(slug=distance_slug)
        results.append(BattleResult(
            stage,
            distance,
            battle,
            athletes1,
            athletes2,
            endurbattle_colour_class1,
            endurbattle_colour_class2,
            ))
    calc_total_gap(
        results,
        endurbattle_colour_class1,
        endurbattle_colour_class2,
        )
    context = {
        'year': year,
        'team1_name': team1_name,
        'team2_name': team2_name,
        'endurbattle_colour_class1': endurbattle_colour_class1,
        'endurbattle_colour_class2': endurbattle_colour_class2,
        'results': results,
        }
    return render(request, 'boltapp/endurrun_superbattle.html', context)


class BattleResult:
    def __init__(
      self,
      stage,
      distance,
      battle,
      athletes1,
      athletes2,
      endurbattle_colour_class1,
      endurbattle_colour_class2,
      ):
        self.stage = stage
        self.distance = distance
        self.athletes1 = athletes1
        self.athletes2 = athletes2
        self.times1 = [x.chiptime for x in self.athletes1 if x.chiptime]
        self.times2 = [x.chiptime for x in self.athletes2 if x.chiptime]
        if len(self.times1) == len(self.times2) and len(self.times1) > 0:
            self.rawgap = (
                sum(self.times1, timedelta()) -
                sum(self.times2, timedelta()))
            if self.rawgap.total_seconds() == 0:
                self.gap = timedelta(seconds=0)
                self.gap_colour_class = 'default'
            elif self.rawgap.total_seconds() < 0:
                self.gap = timedelta(seconds=abs(self.rawgap.total_seconds()))
                self.gap_colour_class = endurbattle_colour_class1
            else:
                self.gap = self.rawgap
                self.gap_colour_class = endurbattle_colour_class2
        else:
            self.gap = False
        self.total_gap = False
        self.total_gap_colour_class = False


class VAthlete:
    def __init__(self, athlete, year_ranking, stage_results):
        self.athlete = athlete
        self.ranking = year_ranking.get(self.athlete, None)
        self.chiptime = stage_results.get(self.athlete.id, None)


def calc_total_gap(
      results,
      endurbattle_colour_class1,
      endurbattle_colour_class2,
      ):
    total_gap_seconds = 0
    keep_going = True
    for r in results:
        if r.gap and keep_going:
            this_gap_seconds = r.gap.total_seconds()
            if r.gap_colour_class == endurbattle_colour_class2:
                this_gap_seconds = this_gap_seconds * -1
            total_gap_seconds += this_gap_seconds
            print(r.stage, total_gap_seconds)
            if total_gap_seconds == 0:
                r.total_gap = timedelta(seconds=0)
                r.total_gap_colour_class = 'default'
            elif total_gap_seconds < 0:
                r.total_gap = timedelta(seconds=abs(total_gap_seconds))
                r.total_gap_colour_class = endurbattle_colour_class2
            else:
                r.total_gap = timedelta(seconds=total_gap_seconds)
                r.total_gap_colour_class = endurbattle_colour_class1
        else:
            keep_going = False
