from django.shortcuts import render
from django.db.models import F, Count
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter, itemgetter
from dateutil.relativedelta import relativedelta
from datetime import date
import urllib
from ..models import *
from ..utils import get_wrdict

def index(request):
    years = 3
    num_results = 30
    named_race = namedtuple('ne', ['total',
                                   'race',
                                   'city',
                                   ])
    sameracedict = {}
    for s in Samerace.objects.all():
        old_race = Race.objects.get(id=s.old_race_id)
        current_race = Race.objects.get(id=s.current_race_id)
        sameracedict[old_race] = current_race
    start_date = date.today() - relativedelta(years=years)
    allresults = Result.objects.select_related().filter(event__date__gt=start_date, event__date__gte=F('athlete__joindate')).exclude(athlete__quitdate__gt=F('event__date')).order_by('-event__date')
    races_dict = {}
    races_city_dict = {}
    for r in allresults:
        race = r.event.race
        if race in sameracedict:
            race = sameracedict[race]
        if race in races_dict:
            races_dict[race] += 1
        else:
            races_city_dict[race] = r.event.city
            races_dict[race] = 1
    sorted_races = sorted(races_dict.items(), key=itemgetter(1), reverse=True)[0:num_results]
    races = []
    for r in sorted_races:
        races.append(named_race(r[1], r[0], races_city_dict[r[0]]))
    events = []
    context = {'races': races,
               'years': years,
              }
    return render(request, 'boltapp/favourite.html', context)
