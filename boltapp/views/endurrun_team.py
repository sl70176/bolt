from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, year, team_slug):
    #qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    namedresult = namedtuple('nr', ['event',
                                    'distance',
                                    'stage',
                                    'athlete',
                                    'colour_class',
                                    'guntime',
                                    'pace',
                                    'fivek_equiv',
                                    'place',
                                    'gender_place',
                                    'category_place',
                                    'category'])
    team = Endurteam.objects.get(slug=team_slug, year=year)
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    dbresults = Result.objects.filter(event__date__icontains=year, event__race__name__icontains='endurrun')
    endurstages = Config.objects.filter(name__icontains='endurstage')
    stage_distances = {}
    for stage in range(1, 8):
        distance_slug = endurstages.get(value=stage).name.split('_', 1)[1]
        distance = Distance.objects.get(slug=distance_slug)
        stage_distances[stage] = distance
    results = []
    for i in range(1,8):
        ath_expr = 'team.st{}'.format(i)
        athlete = eval(ath_expr)
        if athlete:
            if athlete.gender == 'F':
                colour_class = female_colour_class
            else:
                colour_class = male_colour_class
        else:
            colour_class = False
        distance = stage_distances[i]
        try:
            result = dbresults.get(event__distance=distance, athlete=athlete)
        except:
            result = False
        if result:
            event = result.event
            distance = event.distance
            guntime = (result.guntime - 
                       timedelta(microseconds=result.guntime.microseconds))
            place = result.place
            pace = get_pace(guntime, event.distance.km)
            fivek_equiv = str(get_5k_equiv(guntime, event.distance.c25k)).lstrip('0:')
            if result.gender_place is not None:
                gender_place = result.gender_place
            else:
                gender_place = '' 
            if result.category_place is not None:
                category_place = result.category_place
            else:
                category_place = '' 
            category = result.category.name
        else:
            event = False
            guntime = ''
            fivek_equiv = ''
            place = ''
            gender_place = ''
            category_place = ''
            category = ''
            pace = ''
            fivek_quiv= ''
              
        stage = config_dict['endurstage_{}'.format(stage_distances[i].slug)]
        results.append(namedresult(event,
                                   distance,
                                   stage,
                                   athlete,
                                   colour_class,
                                   guntime,
                                   pace,
                                   fivek_equiv,
                                   place,
                                   gender_place,
                                   category_place,
                                   category))
    context = {'results': results,
               'team': team,
               'year': year}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/endurrun_team.html', context)
