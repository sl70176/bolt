from django.http import JsonResponse
from boltapp import tasks
import urllib
from ..models import *
import logging
logger = logging.getLogger(__name__)

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    notifykey = Config.objects.filter(name='notifykey')[0].value
    response = {'result': 'fail',
                'message': 'correct notification key not sent'}
    if 'notifykey' in qstring:
        if qstring['notifykey'][0] == notifykey:
            logger.info('Update multisport notification received')
            tasks.call_multisport_update.delay()
            response = {'result': 'Success',
                        'message': 'Thank you, come again.'}
    return JsonResponse(response)
