from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, segment_id):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    else:
        gender = 'FM'
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    segment = Stravasegment.objects.get(id=segment_id)
    km = '{:.2f}'.format(segment.distance / 1000)
    athletes = Athlete.objects.filter(quitdate=None, isguest=False)
    athlete_dict = {}
    for a in athletes:
        athlete_dict[a.stravaid] = a
    dbleaders = Stravaleaderboard.objects.filter(stravasegment=segment)
    namedresult = namedtuple('nr', ['athlete', 'segtime', 'pace', 'colour_class', 'seconds', 'rank'])
    results = []
    for d in dbleaders:
        if d.gender not in gender:
            continue
        if d.athlete_id in athlete_dict:
            athlete = athlete_dict[d.athlete_id]
            rawsegtime = timedelta(seconds=d.seconds)
            segtime = str(rawsegtime).lstrip('0:')
            pace = get_pace(rawsegtime, segment.distance / 1000)
            if athlete.gender == 'F':
                colour_class = female_colour_class
            else:
                colour_class = male_colour_class
            results.append(namedresult(athlete, segtime, pace, colour_class, d.seconds, d.rank))
    results = sorted(results, key=attrgetter('rank'))
    results = sorted(results, key=attrgetter('seconds'))
    context = {'results': results,
               'segment': segment,
               'km': km,
               'gender': gender,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/strava_segment.html', context)
