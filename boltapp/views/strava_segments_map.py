from django.shortcuts import render
from django.http import HttpResponse
from collections import namedtuple
from ..models import *

def index(request):
    #qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    #message = 'Coming soon!'
    #context = {'message': message}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/strava_segments_map.html')
