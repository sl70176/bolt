from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Min
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, segment_set):
    namedcr = namedtuple('nc', ['segment_name', 'segment_id', 'km', 'queen', 'queen_slug', 'queen_pace',
                         'king', 'king_slug', 'king_pace', 'colour_class'])
    crs = []
    if segment_set == 'top-40':
        segment_set_name = 'Top 40'
        segments = (Stravasegment.objects
                                 .filter(istopx=True)
                                 .order_by('-effort_count')[:40])
    elif segment_set == 'next-40':
        segment_set_name = 'Next 40'
        segments = (Stravasegment.objects
                                 .filter(istopx=True)
                                 .order_by('-effort_count')[40:80])

    dbclub = Athlete.objects.filter(quitdate=None, stravaid__gt=0).values_list('stravaid', 'name', 'slug')
    clubdict = dict([(a, (b, c)) for a, b, c in dbclub])
    config_dict = dict(Config.objects.values_list('name', 'value'))
    strava0_colour_class = config_dict['strava0_colour_class']
    strava1_colour_class = config_dict['strava1_colour_class']
    strava2_colour_class = config_dict['strava2_colour_class']
    female_count = male_count = 0
    queen_dict = {}
    king_dict = {}
    loop = 1
    for s in segments:
        queen_slug = king_slug = False
        dbqueen = Stravaleaderboard.objects.filter(stravasegment=s, rank=1, gender='F')[:1][0]
        queen = dbqueen.athlete_name
        queen_time = timedelta(seconds=dbqueen.seconds)
        queen_pace = get_pace(queen_time, s.distance / 1000)
        if dbqueen.athlete_id in clubdict:
            queen = clubdict[dbqueen.athlete_id][0]
            queen_slug = clubdict[dbqueen.athlete_id][1]
            female_count += 1
            if dbqueen.athlete_id in queen_dict:
                queen_dict[dbqueen.athlete_id] = (queen_dict[dbqueen.athlete_id][0], queen_dict[dbqueen.athlete_id][1] + 1)
            else:
                queen_dict[dbqueen.athlete_id] = (loop, 1)

        else:
            queen = dbqueen.athlete_name
        dbking = Stravaleaderboard.objects.filter(stravasegment=s, rank=1, gender='M')[:1][0]
        king = dbking.athlete_name
        king_time = timedelta(seconds=dbking.seconds)
        king_pace = get_pace(king_time, s.distance / 1000)
        if dbking.athlete_id in clubdict:
            king = clubdict[dbking.athlete_id][0]
            king_slug = clubdict[dbking.athlete_id][1]
            male_count += 1
            if dbking.athlete_id in king_dict:
                king_dict[dbking.athlete_id] = (king_dict[dbking.athlete_id][0], king_dict[dbking.athlete_id][1] + 1)
            else:
                king_dict[dbking.athlete_id] = (loop, 1)
        else:
            king = dbking.athlete_name
        colour_class = strava0_colour_class
        if queen_slug or king_slug:
            colour_class = strava1_colour_class
        if queen_slug and king_slug:
            colour_class = strava2_colour_class

        km = '{:.2f}'.format(s.distance / 1000)
        crs.append(namedcr(s.name, s.id, km, queen, queen_slug, queen_pace, king, king_slug, king_pace, colour_class))
        loop += 1
    namedtop = namedtuple('nt', ['athlete', 'count'])
    queen_list = []
    for k, v in queen_dict.items():
        queen_list.append([k, v[0], v[1]])
    queen_list = sorted(queen_list, key=lambda x: x[1])
    queen_list = sorted(queen_list, key=lambda x: x[2], reverse=True)[0:3]
    topqueens = []
    for i in queen_list:
        topqueens.append(namedtop(Athlete.objects.get(stravaid=i[0]), i[2]))
    king_list = []
    for k, v in king_dict.items():
        king_list.append([k, v[0], v[1]])
    king_list = sorted(king_list, key=lambda x: x[1])
    king_list = sorted(king_list, key=lambda x: x[2], reverse=True)[0:3]
    topkings = []
    for i in king_list:
        topkings.append(namedtop(Athlete.objects.get(stravaid=i[0]), i[2]))
    female_pct = '{:.1%}'.format(female_count / len(segments))
    male_pct = '{:.1%}'.format(male_count / len(segments))
    overall_pct = '{:.1%}'.format((female_count + male_count) / (len(segments) * 2))
    context = {'segment_set_name': segment_set_name,
               'crs': crs,
               'total_segments': len(segments),
               'female_count': female_count,
               'female_pct': female_pct,
               'male_count': male_count,
               'male_pct': male_pct,
               'overall_pct': overall_pct,
               'topqueens': topqueens,
               'topkings': topkings}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/strava_crs.html', context)
