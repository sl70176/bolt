from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    #qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    #year = False
    #if 'year' in qstring:
    #    year = int(qstring['year'][0])
    #years = Endurathletebattle.objects.values_list('battle_year', flat=True)
    #years = sorted(set(years), reverse=True)
    challenges = Challenge.objects.all().order_by('-event__date')
    #if year:
    #    athlete_battles = athlete_battles.filter(battle_year=year)
    #team_battles = Endurteambattle.objects.all().order_by('-battle_year', 'sortorder')
    #if year:
    #    team_battles = team_battles.filter(battle_year=year)
    context = {'challenges': challenges}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/challenges.html', context)
