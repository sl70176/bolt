from django.shortcuts import render
from django import db
from django.http import HttpResponse
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, year1, athlete1_slug, year2, athlete2_slug):
    #qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    namedresult = namedtuple('nr', ['event',
                                    'stage',
                                    'guntime',
                                    'pace'])
    athletes = []
    athletes.append([Athlete.objects.get(slug=athlete1_slug, isguest=False), year1])
    athletes.append([Athlete.objects.get(slug=athlete2_slug, isguest=False), year2])
    years = [year1, year2]
    config_dict = dict(Config.objects.values_list('name', 'value'))
    endurbattle_colour_class1 = config_dict['endurbattle_colour_class1']
    endurbattle_colour_class2 = config_dict['endurbattle_colour_class2']
    endurstages = Config.objects.filter(name__icontains='endurstage')
    stage_distances = {}
    for stage in range(1, 8):
        distance_slug = endurstages.get(value=stage).name.split('_', 1)[1]
        distance = Distance.objects.get(slug=distance_slug)
        stage_distances[stage] = distance
    results = []
    for a in athletes:
        dbresults = Result.objects.filter(athlete_id=a[0].id, event__date__icontains=a[1], event__race__name__icontains='endurrun').order_by('event__date')
        if a[0].quitdate:
            dbresults = dbresults.filter(event__date__lte=a[0].quitdate)
        athlete_results = []
        for result in dbresults:
        # Skip athletes who quit before this race
            event = result.event
            guntime = (result.guntime - 
                       timedelta(microseconds=result.guntime.microseconds))
            pace = get_pace(guntime, event.distance.km)
            stage = config_dict['endurstage_{}'.format(event.distance.slug)]
            athlete_results.append(namedresult(event,
                                       stage,
                                       guntime,
                                       pace))
        results.append(athlete_results)
    named_combined_result = namedtuple('ncr', ['stage', 'distance', 'event1', 'guntime1', 'pace1', 'event2', 'guntime2', 'pace2', 'stage_gap', 'stage_gap_colour_class','stage_total_gap', 'stage_total_gap_colour_class'])
    combined_results = []
    total_gap = timedelta(seconds=0)
    for i in range(0, 7):
        distance = stage_distances[i+1]
        try:
            stage = results[0][i].stage
            event1 = results[0][i].event
            guntime1 = results[0][i].guntime
            pace1 = results[0][i].pace
        except:
            stage = i + 1
            event1 = False
            guntime1 = False
            pace1 = False
        try:
            event2 = results[1][i].event
            guntime2 = results[1][i].guntime
            pace2 = results[1][i].pace
        except:
            event2 = False
            guntime2 = False
            pace2 = False
        if event1 and event2:
            raw_stage_gap = guntime1 - guntime2
            total_gap += raw_stage_gap
            stage_gap_colour_class = False
            stage_total_gap_colour_class = False
            if raw_stage_gap == timedelta(seconds=0):
                stage_gap_colour_class = 'default'
                stage_gap = timedelta(seconds=0)
            elif raw_stage_gap > timedelta(seconds=0):
                stage_gap_colour_class = endurbattle_colour_class2
                stage_gap = raw_stage_gap
            else:
                stage_gap_colour_class = endurbattle_colour_class1
                stage_gap = abs(raw_stage_gap)
            if total_gap == timedelta(seconds=0):
                stage_total_gap_colour_class = 'default'
                stage_total_gap = timedelta(seconds=0)
            elif total_gap > timedelta(seconds=0):
                stage_total_gap_colour_class = endurbattle_colour_class2
                stage_total_gap = total_gap
            else:
                stage_total_gap_colour_class = endurbattle_colour_class1
                stage_total_gap = abs(total_gap)
        else:
            stage_gap = False
            stage_gap_colour_class = False
            stage_total_gap = False
            stage_total_gap_colour_class = False

        combined_results.append(named_combined_result(stage, distance, event1, guntime1, pace1,
                                                      event2, guntime2, pace2, stage_gap,
                                                      stage_gap_colour_class, stage_total_gap,
                                                      stage_total_gap_colour_class))

    context = {'results': combined_results,
               'athletes': athletes,
               'years': years,
               'endurbattle_colour_class1': endurbattle_colour_class1,
               'endurbattle_colour_class2': endurbattle_colour_class2}
    #print(len(db.connection.queries))   # number of sql queries that happened
    return render(request, 'boltapp/endurrun_battle_athlete.html', context)
