from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Sum
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request, athlete_slug):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    vs_athletes = Athlete.objects.filter(isguest=False, quitdate=None, stravaid__gt=0).order_by('name').exclude(slug=athlete_slug)
    vs_athlete = False
    if 'vs' in qstring:
        vs = qstring['vs'][0]
        vs_athlete = Athlete.objects.get(slug=vs)
    vs_athletes = Athlete.objects.filter(isguest=False, quitdate=None, stravaid__gt=0).order_by('name').exclude(slug=athlete_slug)
    if vs_athlete:
        vs_athletes = vs_athletes.exclude(slug=vs_athlete.slug)
    athlete = Athlete.objects.get(slug=athlete_slug)
    config_dict = dict(Config.objects.values_list('name', 'value'))
    strava0_colour_class = config_dict['strava0_colour_class']
    strava2_colour_class = config_dict['strava2_colour_class']
    results = []
    namedresult = namedtuple('nr', ['segment', 'km', 'segtime',
                                    'pace', 'colour_class', 'vs_segtime',
                                    'vs_pace', 'gap', 'vs_colour_class'])
    dbsegments = Stravasegment.objects.filter(istopx=True).order_by('-effort_count')[:80]
    for s in dbsegments:
        rawkm = s.distance / 1000
        km = '{:.2f}'.format(rawkm)
        try:
            seconds = Stravaleaderboard.objects.get(stravasegment=s, athlete_id=athlete.stravaid).seconds
            rawtime = timedelta(seconds=seconds)
            pace = get_pace(rawtime, rawkm)
            segtime = str(rawtime).lstrip('0:')
            colour_class = strava2_colour_class
        except:
            segtime = ''
            pace = ''
            colour_class = strava0_colour_class
        if vs_athlete:
            try:
                vs_seconds = Stravaleaderboard.objects.get(stravasegment=s, athlete_id=vs_athlete.stravaid).seconds
                vs_rawtime = timedelta(seconds=vs_seconds)
                vs_pace = get_pace(vs_rawtime, rawkm)
                vs_segtime = str(vs_rawtime).lstrip('0:')
                gapseconds = seconds - vs_seconds
                if gapseconds == 0:
                    gap = ''
                    vs_colour_class = False
                else:
                    if segtime == '':
                        vs_colour_class = False
                    elif gapseconds > 0:
                        vs_colour_class = strava0_colour_class
                    else:
                        vs_colour_class = strava2_colour_class
                    gap = str(timedelta(seconds=abs(gapseconds))).lstrip('0:')
                    if len(gap) == 2:
                        gap = '0:' + gap
                    if len(gap) == 1:
                        gap = '0:0' + gap
                #gap = raw_time - vs_time
            except:
                vs_segtime = ''
                vs_pace = ''
                gap = ''
                vs_colour_class = False
        else:
            vs_segtime = ''
            vs_pace = ''
            gap = ''
            vs_colour_class = False
        results.append(namedresult(s, km, segtime, pace, colour_class,
                                   vs_segtime, vs_pace, gap, vs_colour_class))
#    athletes = Athlete.objects.filter(quitdate=None, isguest=False, stravaid__gt=0)
#    results = []
#    for a in athletes:
#        if a.gender not in gender:
#            continue
#        dbsegs = Stravaleaderboard.objects.filter(athlete_id=a.stravaid)
#        segment_count = dbsegs.count()
#        if segment_count == 0:
#            continue
#        dbtotal_km = dbsegs.aggregate(Sum('stravasegment__distance'))
#        total_km = dbtotal_km['stravasegment__distance__sum'] / 1000
#        dbtotal_time = dbsegs.aggregate(Sum('seconds'))
#        print(dbtotal_time)
#        total_time = timedelta(seconds=dbtotal_time['seconds__sum'])
#        pace = get_pace(total_time, total_km)
#        if a.gender == 'F':
#            colour_class = female_colour_class
#        else:
#            colour_class = male_colour_class
#        results.append(namedresult(a, segment_count, total_km,
#                                   total_time, pace, colour_class))
#    results = sorted(results, key=attrgetter('total_time'))
#    results = sorted(results, key=attrgetter('total_km'), reverse=True)
#    results = sorted(results, key=attrgetter('segment_count'), reverse=True)
    context = {'athlete': athlete,
               'vs_athletes': vs_athletes,
               'vs_athlete': vs_athlete,
               'results': results}
    return render(request, 'boltapp/strava_conqueror.html', context)
