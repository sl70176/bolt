from django.shortcuts import render
from django.http import HttpResponse
import urllib
import re

from ..models import *

def index(request):
    config_dict = dict(Config.objects.values_list('name', 'value'))
    master_glyph = config_dict['master_glyph']
    d2dred = config_dict['d2dred']
    context = {'master_glyph': master_glyph,
               'd2dred': d2dred}
    return render(request, 'boltapp/about.html', context)
