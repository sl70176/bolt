from django.shortcuts import render
from django import db
from django.http import HttpResponse
from django.db.models import Sum
from collections import namedtuple
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *
from ..utils import *

def index(request):
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    if 'gender' in qstring:
        gender = qstring['gender'][0]
    else:
        gender = 'FM'
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    namedresult = namedtuple('nr', ['athlete', 'segment_count',
                                    'total_km', 'total_time',
                                    'pace', 'colour_class'])
    athletes = Athlete.objects.filter(quitdate=None, isguest=False, stravaid__gt=0)
    top80_segments = list(Stravasegment.objects
                             .filter(istopx=True)
                             .order_by('-effort_count')[:80]
                             .values_list('id', flat=True))
    results = []
    for a in athletes:
        if a.gender not in gender:
            continue
        dbsegs = (Stravaleaderboard.objects
                     .filter(athlete_id=a.stravaid)
                     .filter(stravasegment__in=top80_segments))
        segment_count = dbsegs.count()
        if segment_count == 0:
            continue
        dbtotal_km = dbsegs.aggregate(Sum('stravasegment__distance'))
        total_km = dbtotal_km['stravasegment__distance__sum'] / 1000
        dbtotal_time = dbsegs.aggregate(Sum('seconds'))
        total_time = timedelta(seconds=dbtotal_time['seconds__sum'])
        pace = get_pace(total_time, total_km)
        if a.gender == 'F':
            colour_class = female_colour_class
        else:
            colour_class = male_colour_class
        results.append(namedresult(a, segment_count, total_km,
                                   total_time, pace, colour_class))
    results = sorted(results, key=attrgetter('total_time'))
    results = sorted(results, key=attrgetter('total_km'), reverse=True)
    results = sorted(results, key=attrgetter('segment_count'), reverse=True)
    context = {'results': results,
               'gender': gender,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/strava_conquest.html', context)
