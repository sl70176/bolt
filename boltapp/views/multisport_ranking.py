from django.shortcuts import render
from collections import namedtuple
import itertools
from operator import attrgetter
from datetime import date, timedelta
import urllib
from ..models import *

def index(request):
    named_rank = namedtuple('nr', ['athlete',
                                   'gscore',
                                   'gmouseover',
                                   'oscore',
                                   'omouseover'])
    qstring = urllib.parse.parse_qs(request.META['QUERY_STRING'])
    dbresults = Multisport.objects.select_related()
    if 'date' not in qstring:
        days_expiry = 730
        earliest_date = date.today() - timedelta(days=days_expiry)
        dbresults = dbresults.filter(date__gte=earliest_date)
    if 'gender' in qstring:
        dbresults = dbresults.filter(athlete__gender=qstring['gender'][0])
    results = sorted(list(dbresults),
                     key=attrgetter('athlete_id', 'gs'),
                     reverse=True)
    ranking = []
    for key, group in itertools.groupby(results, attrgetter('athlete_id')):
        athlete_list = list(group)
        if len(athlete_list) >= 3:
            gscore = sum(x.gs for x in athlete_list[0:3]) / 3
            gmouseover = ('\n'.join(['{:.2f} {} {} {}'
                          .format(x.gs, x.date.year, x.race, x.distance) 
                          for x in athlete_list[0:3]]))
            olist = sorted(athlete_list, key=attrgetter('os'), reverse=True)
            oscore = sum(x.os for x in athlete_list[0:3]) / 3
            omouseover = ('\n'.join(['{:.2f} {} {} {}'
                          .format(x.os, x.date.year, x.race, x.distance) 
                          for x in olist[0:3]]))
            ranking.append(named_rank(athlete_list[0].athlete,
                                      gscore,
                                      gmouseover,
                                      oscore,
                                      omouseover))
    ranking = sorted(ranking, key=attrgetter('oscore'), reverse=True)
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    context = {'qstring': qstring,
               'ranking': ranking,
               'female_colour_class': female_colour_class,
               'male_colour_class': male_colour_class}
    return render(request, 'boltapp/multisport_ranking.html', context)
