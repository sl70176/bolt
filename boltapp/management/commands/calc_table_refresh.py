#!/usr/bin/env python
""" calc_table_refresh.py - Refresh calculated tables """
from django.core.management.base import BaseCommand, CommandError
from boltapp.models import * 
from boltapp import calc_index, calc_standards
import sys
import logging
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Refresh calculated tables'
    def add_arguments(self, parser):
        parser.add_argument(
            '--table',
            dest='table',
            default='all',
        )
        parser.add_argument(
            '--distance',
            dest='distance_slug',
            default=False,
        )

    def handle(self, *args, **options):
        table = 'all'
        distance = False
        if options['table']:
            table = options['table']
        if options['distance_slug']:
            distance = Distance.objects.get(slug=options['distance_slug'])
        logger.info('Starting refresh of calculated tables')
        if table in ('all', 'standards'):
            calc_standards.refresh_standard_calculated(distance)
        if table in ('all', 'index'):
            calc_index.refresh_index_calculated()
