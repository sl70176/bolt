#!/usr/bin/env python
""" parseresults.py - Race result parsing utility. """
from django.core.management.base import BaseCommand
from boltapp.models import (
    Athleteracename,
    Category,
    Config,
    Distance,
    Event,
    Race,
    Result,
    Resultexclude,
    Split,
    Standarddef,
)
from boltapp import calc_index, calc_standards
import datetime
import copy
import gspread
import logging

logger = logging.getLogger(__name__)

MASTERS_SUBSTRINGS = [
    "40-",
    "45-",
    "50-",
    "55-",
    "60-",
    "65-",
    "70-",
    "75-",
    "40+",
    "50+",
    "60+",
    "70+",
]


class Command(BaseCommand):
    help = "Adds races to database from pre"

    def add_arguments(self, parser):
        parser.add_argument(
            "-e",
            action="store",
            dest="event",
            default=False,
            help='Event to scrape',
        )

    def handle(self, *args, **options):
        if not options["event"]:
            print("Event not received")
            exit()
        logging.info("Updating event {}".format(options["event"]))
        events = []
        done = False
        events = Event.objects.filter(id=options["event"])
        if len(events) == 0:
            info = "Event(s) not found"
            logger.info(info)
        namelist = Athleteracename.objects.values_list("name", "athlete_id")
        lowernamelist = [(x[0].lower(), x[1]) for x in namelist]
        athleteracenamesdict = dict(lowernamelist)

        # Remove sprint triathlon if found
        orig_length = len(events)
        events = [x for x in events if "Sprint Duathlon" not in x.distance.name]
        new_length = len(events)
        if new_length < orig_length:
            logger.info("Skipping Sprint Duathlon")

        for event in events:
            year = int(event.date.year)
            if year >= 2012:
                process_event(event, athleteracenamesdict)
            else:
                info = "{} {} {} (Event {}) prior to 2012, skipping".format(
                    year, event.race.name, event.distance.name, event.id
                )
                logger.info(info)
        calc_index.refresh_index_calculated()
        call_calc_standards(events)


def process_event(event, athleteracenamesdict):
    exclude_list = list(
        Resultexclude.objects.filter(event_id=event.id).values_list(
            "athlete", "guntime"
        )
    )
    year = event.date.year
    race = event.race
    distance = event.distance
    if event.id == 277:
        distance = Distance.objects.get(slug="21_34-km")
    info = "Starting processing for {} {} {} (Event {})".format(
        year, race.name, distance.name, event.id
    )
    logger.info(info)
    genderdict = {"F": 0, "M": 0, "": 0}
    categorydict = {}
    hasresults = False
    results = []
    splits = []
    c25k = distance.c25k
    google_results = get_results_from_google(event.resultsurl)
    for result in google_results:
        extra_dict = {}
        for k, v in result.items():
            if "split" in k:
                if v:
                    extra_dict["k"] = v
        skip = False
        resultcategory = ""
        if "category" in result:
            resultcategory = result["category"]
        if int(result["place"]) > 990000:
            continue
        gender = result["gender"]
        if gender not in ("M", "F"):
            continue
        genderdict[gender] += 1
        if gender in ("F", "M"):
            if "gender_place" in result:
                gender_place = result["gender_place"]
            else:
                gender_place = genderdict[gender]
        else:
            gender_place = None
        if resultcategory in categorydict:
            categorydict[resultcategory] += 1
        else:
            categorydict[resultcategory] = 1
        if "category_place" in result:
            category_place = result["category_place"]
        else:
            category_place = categorydict[resultcategory]
        if "athlete" in result:
            result["name"] = result["athlete"]
        if result["name"].lower() in athleteracenamesdict:
            athlete = athleteracenamesdict[result["name"].lower()]
        else:
            continue
        if result["guntime"]:
            guntime = maketimedelta(result["guntime"])
        else:
            guntime = False
        if ((athlete, guntime)) in exclude_list:
            skip = True
        if skip:
            continue
        try:
            category = Category.objects.get(name=resultcategory)
        except Exception:
            ismasters = False
            for i in MASTERS_SUBSTRINGS:
                if i in resultcategory:
                    ismasters = True
            category = Category(name=resultcategory, ismasters=ismasters)
            category.save()
        if "chiptime" in result:
            chiptime = maketimedelta(result["chiptime"])
        else:
            chiptime = False
        if not guntime:
            guntime = chiptime
        if not chiptime:
            chiptime = guntime
        if chiptime > guntime:
            chiptime = guntime
        total_chip_seconds = chiptime.total_seconds()
        fivek_equiv = datetime.timedelta(
            seconds=round(int(total_chip_seconds) * float(c25k))
        )
        splits = add_splits(event, result, extra_dict, splits, event)
        newresult = Result(
            event_id=event.id,
            place=result["place"],
            bib=result["bib"],
            athlete_id=athlete,
            category=category,
            city=result["city"],
            chiptime=chiptime,
            guntime=guntime,
            fivek_equiv=fivek_equiv,
            gender_place=gender_place,
            gender_total=0,
            category_place=category_place,
            category_total=0,
        )
        results.append(newresult)


    # WEIRD WORKAROUND FOR TEAM CHALLENGE #
    if event.id == 310:  # add placeholder result for 2016 oktoberfest 10k
        athlete = athleteracenamesdict["Placeholder (AM)".lower()]
        category = Category.objects.get(name="")
        chiptime = datetime.timedelta(seconds=2340)
        fivek_equiv = datetime.timedelta(
            seconds=round(int(total_chip_seconds) * float(c25k))
        )
        newresult = Result(
            event_id=event.id,
            place=0,
            bib=0,
            athlete_id=athlete,
            category=category,
            city="",
            chiptime=chiptime,
            guntime=None,
            fivek_equiv=fivek_equiv,
            gender_place=0,
            gender_total=0,
            category_place=0,
            category_total=0,
        )
        results.append(newresult)
    # END WEIRD WORKAROUND #

    # Delete any existing results for this event
    Result.objects.filter(event_id=event.id).delete()
    for result in results:
        if gender_place:
            try:
                gender_total = gender_total
            except Exception:
                gender_total = genderdict[result.athlete.gender]
        else:
            gender_total = None
        if category_place:
            try:
                category_total = category_total
            except Exception:
                try:
                    category_total = categorydict[result.category.name]
                except Exception:
                    category_total = 0
        else:
            category_total = None
        if gender_total == 0:
            result.gender_total = None
        else:
            result.gender_total = gender_total
        if category_total == "":
            result.category_total = None
        else:
            result.category_total = category_total
        if result.gender_place == "":
            result.gender_place = None
        if result.category_place == "":
            result.category_place = None
        result.save()
    info = "{} results processed for {} {} {} (Event {})".format(
        len(results), year, race.name, distance.name, event.id
    )
    logger.info(info)
    Split.objects.filter(event_id=event.id).delete()
    if len(splits) > 0:
        Split.objects.bulk_create(splits)
        info = "{} splits processed for {} {} {} (Event {})".format(
            len(splits), year, race.name, distance.name, event.id
        )
        logger.info(info)
    if results:
        event.hasresults = True
        event.save()


def maketimedelta(strtime):
    if strtime == "":
        return None
    if "." in strtime:
        microsec = strtime.split(".")[1]
        if int(microsec) == 0:
            milliseconds = 0
        else:
            if microsec[0] == 0:
                milliseconds = int(microsec) / 10000
            else:
                milliseconds = int(microsec) / 1000
        hours, minutes, seconds = strtime.split(".")[0].split(":")
        if " " in hours:
            daypart, hourpart = hours.split(" ")
            hours = 24 * int(daypart) + int(hourpart)
    else:
        milliseconds = 0
        if len(strtime.split(":")) == 3:
            hours, minutes, seconds = strtime.split(":")
            if " " in hours:
                daypart, hourpart = hours.split(" ")
                hours = 24 * int(daypart) + int(hourpart)
        else:
            hours = 0
            minutes, seconds = strtime.split(":")
    timedelta = datetime.timedelta(
        hours=int(hours),
        minutes=int(minutes),
        seconds=int(seconds),
        milliseconds=milliseconds,
    )
    return timedelta


def add_splits(event, result, extra_dict, splits, e):
    """ Add any splits """
    these_splits = []
    for k, v in extra_dict.items():
        if "split" not in k:
            continue
        split_num = int(k.strip("split"))
        split_time = maketimedelta(v)
        this_split = Split(
            event_id=event.id,
            place=result["place"],
            split_num=split_num,
            split_time=split_time,
        )
        these_splits.append(this_split)
    if e.race.slug == "around-the-bay":
        these_splits = atb_splits(these_splits, result)
    if e.race.slug == "sporting-life-toronto":
        these_splits = slt_splits(these_splits, result)
    for i in these_splits:
        splits.append(i)
    return splits


def call_calc_standards(events):
    if len(events) == 0:
        return False
    elif len(events) == 1:
        distance = events[0].distance
        standards_distances = list(
            set(Standarddef.objects.values_list("distance", flat=True))
        )
        if distance.id in standards_distances:
            calc_standards.refresh_standard_calculated(distance)
        else:
            logger.info("Standards recalculation not required, non-standard distance")
        return True
    else:
        calc_standards.refresh_standard_calculated(False)
        return True


def atb_splits(splits, result):
    atb_splits = []
    split1 = [x for x in splits if x.split_num == 1]
    split2 = [x for x in splits if x.split_num == 2]
    split3 = [x for x in splits if x.split_num == 3]
    chiptime = maketimedelta(result["chiptime"])
    if len(split3) == 1:
        this_split = split3[0]
        try:
            split3_time = chiptime - split3[0].split_time
        except Exception:
            split3_time = None
        this_split.split_num = 3
        this_split.split_time = split3_time
        atb_splits.append(this_split)
    if len(split3) == 1 and len(split1) == 1:
        this_split = split2[0]
        try:
            split2_time = chiptime - split3[0].split_time - split1[0].split_time
        except Exception:
            split2_time = None
        this_split.split_num = 2
        this_split.split_time = split2_time
        atb_splits.append(this_split)
    if len(split1) == 1:
        atb_splits.append(split1[0])
    atb_splits.reverse()
    return atb_splits


def slt_splits(splits, result):
    slt_splits = []
    split1 = [x for x in splits if x.split_num == 1]
    chiptime = maketimedelta(result["chiptime"])
    if len(split1) == 1:
        first_split = split1[0]
        slt_splits.append(first_split)
        second_split = copy.copy(first_split)
        second_split.split_num = 2
        second_split.split_time = chiptime - first_split.split_time
        slt_splits.append(second_split)
    return slt_splits


def get_results_from_google(url):
    """Grab results from a Google Sheet"""
    if "docs.google.com" not in url:
        logging.error("Not a Google Sheets URL")
        exit(1)
    gc = gspread.service_account("/home/bolt/google_service_account.json")
    sh = gc.open_by_url(url)
    try:
        worksheet = sh.worksheet("individual")
    except:
        worksheet = sh.worksheet("Sheet1")
    results = worksheet.get_all_records()
    return results
