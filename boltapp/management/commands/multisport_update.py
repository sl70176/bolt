#!/usr/bin/env python
""" multisport_update.py - Update multisport results from Google sheet """
from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify
from boltapp.models import *
from boltapp.management.commands.addraces import maketimedelta
import os
import datetime
import json
import sys
import logging
import gspread
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update multisport results from Google sheet'

    def handle(self, *args, **options):
        logger.info('Starting multisport update')
        google_sheets_url = Config.objects.get(name="multisport_google_sheet").value
        apiresults = get_results_from_google(google_sheets_url)
        logger.info('{} results found for processing'.format(len(apiresults)))
        namelist = (Athleteracename
                    .objects
                    .values_list
                    ('name', 'athlete_id')
                   )
        lowernamelist = [ (x[0].lower(), x[1]) for x in namelist ]
        athleteracenamesdict = dict(lowernamelist)
        results = []
        for r in apiresults:
            if r['athlete'].lower() in athleteracenamesdict:
                athlete_id = athleteracenamesdict[r['athlete'].lower()]
            else:
                logger.info('Athlete not found for {}'.format(r))
                continue
            if r["time"] == "":
                rawtime = "00:00:00"
            else:
                rawtime = r["time"]
            time = maketimedelta(rawtime)
            if r["category_finishers"] == "":
                r["category_finishers"] = None
            if r["category_place"] == "":
                r["category_place"] = None
            if r["gender_finishers"] == "":
                r["gender_finishers"] = None
            if r["gender_place"] == "":
                r["gender_place"] = None
            event_slug = ('{}-{}-{}'.format(r['date'],
                                            slugify(r['race']),
                                            slugify(r['distance'])))
            thisresult=Multisport(
                athlete=Athlete.objects.get(id=athlete_id),
                date=r['date'],
                event_type=r['event_type'],
                distance=r['distance'],
                race=r['race'],
                time=time,
                place=r['place'],
                finishers=r['finishers'],
                gender_place=r['gender_place'],
                gender_finishers=r['gender_finishers'],
                category=r['category'],
                category_place=r['category_place'],
                category_finishers=r['category_finishers'],
                resultsurl=r['resultsurl'],
                bike_type=r['bike_type'],
                event_slug=event_slug,
                 )
            results.append(thisresult)
        Multisport.objects.all().delete()
        Multisport.objects.bulk_create(results)
        logger.info('Finished processing multisport results')

def get_results_from_google(url):
    """Grab results from a Google Sheet"""
    gc = gspread.service_account("/home/bolt/google_service_account.json")
    sh = gc.open_by_url(url)
    worksheet = sh.worksheet("Sheet 1")
    results = worksheet.get_all_records()
    return results
