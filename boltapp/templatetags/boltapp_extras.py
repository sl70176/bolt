from django import template
from datetime import timedelta
from time import strftime
from ..models import Config

register = template.Library()

@register.filter(name='get_time')
def get_time(orig_time):
    """ Truncate time and handle other weirdness """
    try:
        clean_time = orig_time - timedelta(microseconds=orig_time.microseconds)
    except:
        clean_time = ''
    else:
        if clean_time.total_seconds() >= 86400:
            hours, remainder = divmod(clean_time.total_seconds(), 3600)
            minutes, seconds = divmod(remainder, 60)
            hours = str(int(hours))
            minutes = str(int(minutes))
            seconds = str(int(seconds))
            if len(minutes) == 1:
                minutes = '0{}'.format(minutes)
            if len(seconds) == 1:
               seconds = '0{}'.format(seconds)
            clean_time = '{}:{}:{}'.format(hours, minutes, seconds)
    return clean_time

@register.filter(name='get_speed')
def get_time(orig_time, distance):
    """ Calculate speed """
    speed = distance / orig_time.total_seconds() * 3600
    return speed
