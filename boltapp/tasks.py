from __future__ import absolute_import
from django.core.management import call_command
from celery import shared_task
from boltapp import post_hook, add_racename, calc_index, calc_standards

@shared_task
def call_post_hook():
    post_hook.index()
    return True

@shared_task
def call_addraces(event_id):
    call_command('addraces', '-e', event_id)
    return True

@shared_task
def call_add_racename(athlete_id, athlete_name):
    add_racename.index(athlete_id, athlete_name)
    return True

@shared_task
def call_calc_standards(distance):
    calc_standards.refresh_standard_calculated(distance)
    return True

@shared_task
def call_calc_index():
    calc_index.refresh_index_calculated()
    return True

@shared_task
def call_multisport_update():
    call_command('multisport_update')
    return True
# Example task
#@shared_task
#def test(param):
#    return 'The test task executed with argument {}'.format(param)
