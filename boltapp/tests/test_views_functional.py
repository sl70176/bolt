from django.test import Client, TestCase
import os


def setUpModule():
    """ Import the actual database """
    os.system("sudo mysqldump bolt | sudo mysql test_bolt")


class SimpleTest(TestCase):
    """ Functional test of views """

    def setUp(self):
        self.client = Client()

    def test_root_view_response_code(self):
        self.response = self.client.get("/")
        self.assertEqual(self.response.status_code, 200)

    def test_index_view_response_code(self):
        self.response = self.client.get("/index")
        self.assertEqual(self.response.status_code, 200)

    def test_ranking_view_response_code(self):
        self.response = self.client.get("/ranking")
        self.assertEqual(self.response.status_code, 200)

    def test_challenge_view_response_code(self):
        self.response = self.client.get(
            "/challenge/nick-vs-jordan/2016/waterloo-classic/10-km/"
        )
        self.assertEqual(self.response.status_code, 200)
