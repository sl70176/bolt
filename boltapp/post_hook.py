from django.core.management import call_command
import fileinput
import os
import pip
import socket
import logging
logger = logging.getLogger(__name__)

def index():
    prod_hostname = 'hagen'
    hostname = socket.gethostname()
    if prod_hostname in hostname:
        GIT_DIR = '/srv/bolt/'
        WSGI_PATH = GIT_DIR + 'bolt/wsgi.py'
        SETTINGS_PATH = GIT_DIR + 'bolt/settings.py'
        REQUIREMENTS = GIT_DIR + 'requirements.txt'
        logger.info('Starting post hook commands')
        logger.info('Pulling from git')
        os.system('cd {}; git fetch --all'.format(GIT_DIR))
        os.system('cd {}; git reset --hard origin/master'.format(GIT_DIR))
        logger.info('Installing pip requirements')
        pip.main(['install', '-r', REQUIREMENTS])
        logger.info('Setting debug to False')
        with fileinput.FileInput(SETTINGS_PATH, inplace=True) as file:
            for line in file:
                print(line.replace('DEBUG = True', 'DEBUG = False'), end='')
        logger.info('Collecting static files')
        call_command('collectstatic', verbosity=0, interactive=False)
        #logger.info('Making files group writeable')
        #os.system('chmod -R g+w {}'.format(GIT_DIR))
        logger.info('Touching wsgi.py')
        os.system('touch {}'.format(WSGI_PATH))
        logger.info('Finished post hook commands')
    else:
        logger.error('Post hook only runs in production (on {})'.format(prod_hostname))
