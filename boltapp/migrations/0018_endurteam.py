# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-13 16:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0017_auto_20160308_1421'),
    ]

    operations = [
        migrations.CreateModel(
            name='Endurteam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.IntegerField()),
                ('name', models.CharField(max_length=128)),
                ('slug', models.SlugField()),
                ('st1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st1', to='boltapp.Athlete')),
                ('st2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st2', to='boltapp.Athlete')),
                ('st3', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st3', to='boltapp.Athlete')),
                ('st4', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st4', to='boltapp.Athlete')),
                ('st5', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st5', to='boltapp.Athlete')),
                ('st6', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st6', to='boltapp.Athlete')),
                ('st7', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='st7', to='boltapp.Athlete')),
            ],
        ),
    ]
