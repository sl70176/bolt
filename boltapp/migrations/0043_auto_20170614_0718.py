# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-14 11:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0042_auto_20170614_0718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='endurteam',
            name='name',
            field=models.CharField(help_text='Match previous-year same team name exactly.', max_length=128),
        ),
    ]
