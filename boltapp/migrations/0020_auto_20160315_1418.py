# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-15 18:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0019_endurathletebattles'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Endurathletebattles',
            new_name='Endurathletebattle',
        ),
    ]
