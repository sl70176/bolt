# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-09-12 00:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0031_auto_20160720_1622'),
    ]

    operations = [
        migrations.CreateModel(
            name='Samerace',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('current_race', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='current_race', to='boltapp.Race')),
                ('old_race', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='old_race', to='boltapp.Race')),
            ],
        ),
    ]
