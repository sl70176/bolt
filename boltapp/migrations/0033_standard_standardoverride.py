# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-20 02:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0032_samerace'),
    ]

    operations = [
        migrations.CreateModel(
            name='Standard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gender', models.CharField(choices=[('F', 'Female'), ('M', 'Male')], max_length=1)),
                ('ismasters', models.BooleanField()),
                ('letter', models.CharField(choices=[('A', 'A'), ('B', 'B'), ('C', 'C')], max_length=1)),
                ('guntime', models.DurationField()),
                ('distance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Distance')),
            ],
        ),
        migrations.CreateModel(
            name='Standardoverride',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ispb', models.BooleanField()),
                ('athlete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Athlete')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Event')),
                ('standard', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Standard')),
            ],
        ),
    ]
