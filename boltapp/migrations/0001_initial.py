# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-22 21:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Athlete',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
                ('slug', models.SlugField(unique=True)),
                ('gender', models.CharField(max_length=1)),
                ('dob', models.DateField(blank=True, null=True)),
                ('joindate', models.DateField()),
                ('quitdate', models.DateField(blank=True, null=True)),
                ('stravaid', models.IntegerField(blank=True, null=True)),
                ('isguest', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Athleteracenames',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
                ('athlete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Athlete')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, unique=True)),
                ('ismasters', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Config',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('value', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Distance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('prename', models.CharField(max_length=25)),
                ('name', models.CharField(max_length=25, unique=True)),
                ('slug', models.SlugField(unique=True)),
                ('km', models.DecimalField(decimal_places=5, max_digits=9)),
                ('c25k', models.DecimalField(decimal_places=10, max_digits=12)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('distance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Distance')),
            ],
            options={
                'ordering': ('-date', '-distance__km'),
            },
        ),
        migrations.CreateModel(
            name='Race',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('prename', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50, unique=True)),
                ('shortname', models.CharField(max_length=50)),
                ('slug', models.SlugField(unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bib', models.CharField(blank=True, max_length=6)),
                ('city', models.CharField(max_length=50)),
                ('place', models.IntegerField()),
                ('gender_place', models.IntegerField()),
                ('gender_total', models.IntegerField()),
                ('category_place', models.IntegerField()),
                ('category_total', models.IntegerField()),
                ('chiptime', models.DurationField(null=True)),
                ('guntime', models.DurationField(null=True)),
                ('athlete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Athlete')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Category')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Event')),
            ],
            options={
                'ordering': ('place',),
            },
        ),
        migrations.AddField(
            model_name='event',
            name='race',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Race'),
        ),
        migrations.AlterUniqueTogether(
            name='result',
            unique_together=set([('event', 'place')]),
        ),
        migrations.AlterUniqueTogether(
            name='event',
            unique_together=set([('race', 'distance', 'date')]),
        ),
    ]
