# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-14 11:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0039_auto_20170612_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='athlete',
            name='slug',
            field=models.SlugField(help_text='Name in lower case, drop special characters, replace spaces with hyphens.', unique=True),
        ),
    ]
