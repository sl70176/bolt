# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-09 16:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0028_challengeteam'),
    ]

    operations = [
        migrations.CreateModel(
            name='Challengemembers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('athlete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Athlete')),
                ('challengeteam', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boltapp.Challengeteam')),
            ],
        ),
    ]
