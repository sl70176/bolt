# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-20 03:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0033_standard_standardoverride'),
    ]

    operations = [
        migrations.RenameField(
            model_name='standard',
            old_name='guntime',
            new_name='time',
        ),
    ]
