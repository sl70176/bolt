# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-08 19:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boltapp', '0016_auto_20160307_2139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='config',
            name='value',
            field=models.CharField(max_length=100),
        ),
    ]
