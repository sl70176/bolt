from datetime import datetime, date
from .utils import get_age
from .models import *
import logging
logger = logging.getLogger(__name__)

def refresh_standard_calculated(distance):
    standards_start_date = Config.objects.get(name='standards_start_date').value
    allresults = (Result.objects.select_related()
                  .filter(event__date__gte=standards_start_date,
                          athlete__isguest=False)
                  .order_by('event__date'))
    if distance:
        logger.info('Refreshing standard_calculated table for distance: {}'.format(distance.name))
        standard_defs = Standarddef.objects.filter(distance=distance)
        standardmanuals = Standardmanual.objects.filter(event__distance=distance)
        results = allresults.filter(event__distance=distance)
    else:
        logger.info('Refreshing standard_calculated table for all distances')
        standard_defs = Standarddef.objects.all()
        standardmanuals = Standardmanual.objects.all()
        standards_distances = list(set(Standarddef.objects.values_list('distance', flat=True)))
        results = allresults.filter(event__distance__in=standards_distances)
    standard_dict = {}
    for s in standard_defs:
        this_master = 'any'
        if s.ismasters:
            this_master = 'master'
        standard_dict['{}__{}__{}__{}'.format(s.distance.slug, s.gender, this_master, s.letter)] =  [s, s.time]
    athlete_standard_dict = {}
    standards = []
    for m in standardmanuals:
        manual_master = 'any'
        if m.standarddef.ismasters:
            manual_master = 'master'
        athlete_standard = '{}__{}__{}__{}'.format(m.athlete.slug, m.event.distance.slug, manual_master, m.standarddef.letter)
        ispb = False
        if m.ispb:
            athlete_standard += '_pb'
            ispb = True
        athlete_standard_dict[athlete_standard] = m.event
        standards.append(Standardcalc(ispb=ispb,
                                      athlete=m.athlete,
                                      event=m.event,
                                      standarddef=m.standarddef))
    for r in results:
        # exclude results after people quit
        if r.athlete.quitdate:
            if r.event.date >= r.athlete.quitdate:
                continue
        # exclude results before people join
        if r.event.date < r.athlete.joindate:
            continue
        that_master = 'any'
        athlete_age = get_age(r.event.date, r.athlete.dob)
        if athlete_age >= 40:
            that_master = 'master'
        for letter in ['A', 'B', 'C']:
            athlete_standard = '{}__{}__{}__{}'.format(r.athlete.slug, r.event.distance.slug, that_master, letter)
            if athlete_standard in athlete_standard_dict:
                break
            eligible_standard = '{}__{}__{}__{}'.format(r.event.distance.slug, r.athlete.gender, that_master, letter)
            time_to_beat = standard_dict[eligible_standard][1]
            if r.chiptime < time_to_beat:
                athlete_standard_dict[athlete_standard] = [r.event]
                standards.append(Standardcalc(ispb=False,
                                              athlete=r.athlete,
                                              event=r.event,
                                              standarddef=standard_dict[eligible_standard][0]))
                break
    if distance:
        Standardcalc.objects.filter(event__distance=distance).delete()
    else:
        Standardcalc.objects.all().delete()
    Standardcalc.objects.bulk_create(standards)
    logger.info('Finished calculating standards')
