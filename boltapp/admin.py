from django.contrib import admin
from .models import (
    Agefilter,
    Athlete,
    Athleteracename,
    Challenge,
    Challengemember,
    Challengeteam,
    Config,
    Distance,
    Event,
    Endurathletebattle,
    Endurteam,
    Endurteambattle,
    Endurteamsuperbattle,
    Race,
    Raceseries,
    Result,
    Standardcalc,
    Standarddef,
    Standardmanual,
    Stravaleaderboard,
    Stravasegment,
)


class ConfigAdmin(admin.ModelAdmin):
    list_display = ("name", "value")
    search_fields = ("name",)
    ordering = ("name",)


admin.site.register(Config, ConfigAdmin)


class AthleteAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "gender",
        "dob",
        "joindate",
        "quitdate",
        "stravaid",
        "isguest",
    )
    search_fields = ("name",)
    ordering = ("name",)


admin.site.register(Athlete, AthleteAdmin)


class AthleteracenameAdmin(admin.ModelAdmin):
    list_display = ("athlete", "name")
    search_fields = ("athlete__name",)
    ordering = ("name",)


admin.site.register(Athleteracename, AthleteracenameAdmin)


class DistanceAdmin(admin.ModelAdmin):
    list_display = ("name", "km", "slug", "abbr", "c25k", "ispb")
    search_fields = ("name",)
    ordering = ("km",)


admin.site.register(Distance, DistanceAdmin)


class RaceAdmin(admin.ModelAdmin):
    list_display = ("name", "shortname", "slug", "series")
    search_fields = ("name",)
    ordering = ("name",)


admin.site.register(Race, RaceAdmin)


class RaceseriesAdmin(admin.ModelAdmin):
    list_display = ("name", "slug")
    search_fields = ("name",)
    ordering = ("name",)


admin.site.register(Raceseries, RaceseriesAdmin)


class ResultAdmin(admin.ModelAdmin):
    list_display = (
        "event",
        "place",
        "athlete",
        "category",
        "bib",
        "city",
        "gender_place",
        "gender_total",
        "category_place",
        "category_total",
        "chiptime",
        "guntime",
    )
    search_fields = ("name",)
    ordering = ("-event__date", "place")


admin.site.register(Result, ResultAdmin)


class AgefilterAdmin(admin.ModelAdmin):
    list_display = ("name", "slug", "minage", "maxage", "sortorder")
    ordering = ("sortorder",)


admin.site.register(Agefilter, AgefilterAdmin)


class StravasegmentAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "distance",
        "average_grade",
        "effort_count",
        "athlete_count",
        "istopx",
    )
    search_fields = ("id",)


admin.site.register(Stravasegment, StravasegmentAdmin)


class StravaleaderboardAdmin(admin.ModelAdmin):
    list_display = (
        "stravasegment",
        "rank",
        "athlete_id",
        "athlete_name",
        "gender",
        "seconds",
    )
    search_fields = ("stravasegment__id", "athlete_name")


admin.site.register(Stravaleaderboard, StravaleaderboardAdmin)


class EndurteamAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "year",
        "name",
        "slug",
        "st1",
        "st2",
        "st3",
        "st4",
        "st5",
        "st6",
        "st7",
    )
    search_fields = ("name",)
    ordering = ("-year", "id")


admin.site.register(Endurteam, EndurteamAdmin)


class EndurathletebattleAdmin(admin.ModelAdmin):
    list_display = (
        "battle_year",
        "athlete1_year",
        "athlete1",
        "athlete2_year",
        "athlete2",
        "sortorder",
    )
    ordering = ("-battle_year", "sortorder")


admin.site.register(Endurathletebattle, EndurathletebattleAdmin)


class EndurteambattleAdmin(admin.ModelAdmin):
    list_display = ("battle_year", "team1", "team2", "sortorder")
    ordering = ("-battle_year", "sortorder")


admin.site.register(Endurteambattle, EndurteambattleAdmin)


class EndurteamsuperbattleAdmin(admin.ModelAdmin):
    list_display = ("battle_year", "team1_name", "team2_name", "sortorder")
    ordering = ("-battle_year", "sortorder")


admin.site.register(Endurteamsuperbattle, EndurteamsuperbattleAdmin)


class EventAdmin(admin.ModelAdmin):
    list_display = ("id", "date", "race", "distance")
    search_fields = ("race__name",)
    ordering = ("-date",)


admin.site.register(Event, EventAdmin)


class ChallengeAdmin(admin.ModelAdmin):
    list_display = ("event", "name", "slug", "count")
    search_fields = ("name",)
    ordering = ("event__date", "id")


admin.site.register(Challenge, ChallengeAdmin)


class ChallengeteamAdmin(admin.ModelAdmin):
    list_display = ("challenge", "name")
    search_fields = ("name",)
    ordering = ("challenge", "id")


admin.site.register(Challengeteam, ChallengeteamAdmin)


class ChallengememberAdmin(admin.ModelAdmin):
    list_display = ("challengeteam", "athlete")
    search_fields = ("challengeteam",)
    ordering = (
        "-challengeteam__challenge__event__date",
        "challengeteam__challenge",
        "challengeteam",
        "id",
    )


admin.site.register(Challengemember, ChallengememberAdmin)


class StandarddefAdmin(admin.ModelAdmin):
    list_display = ("letter", "distance", "gender", "ismasters", "time")
    ordering = ("letter", "distance", "ismasters", "-gender")


admin.site.register(Standarddef, StandarddefAdmin)


class StandardmanualAdmin(admin.ModelAdmin):
    list_display = ("athlete", "event", "standarddef", "ispb")


admin.site.register(Standardmanual, StandardmanualAdmin)


class StandardcalcAdmin(admin.ModelAdmin):
    list_display = ("athlete", "event", "standarddef", "ispb")


admin.site.register(Standardcalc, StandardcalcAdmin)
