from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from django.conf import settings
from boltapp.models import *
#from boltapp import calc_index, calc_standards
from boltapp import tasks
import os.path
 
 
@receiver(post_save, sender=Athleteracename)
@receiver(post_save, sender=Athlete)
@receiver(post_save, sender=Config)
@receiver(post_save, sender=Standarddef)
@receiver(post_save, sender=Standardmanual)
def model_post_save(sender, **kwargs):
    #print('Saved: {}'.format(kwargs['instance'].__dict__))
    if 'boltapp.models.Athleteracename' in str(sender):
        racename = kwargs['instance'].name
        tasks.call_addraces.delay(racename=racename)
    elif 'boltapp.models.Athlete' in str(sender):
        athlete_id = kwargs['instance'].id
        athlete_name = kwargs['instance'].name
        tasks.call_add_racename.delay(athlete_id, athlete_name)
        tasks.call_calc_standards.delay(False)
        tasks.call_calc_index.delay()
