from django import db
from django.db.models import Min   
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
from collections import namedtuple
from operator import attrgetter
from datetime import datetime, date
from calendar import monthrange
import itertools
from .models import *

def truncate_time(time):
    try:
        int(time)
    except:
        trunc_time = time - timedelta(microseconds=time.microseconds)
    else:
        ustime = timedelta(microseconds = time)
        trunc_time = ustime - timedelta(microseconds=ustime.microseconds)
    return trunc_time

def get_pace(guntime, distance):
    if distance == 0:
        pace = ''
    else:
        paceseconds = guntime.total_seconds() / float(distance)
        rawpace = timedelta(seconds = round(paceseconds,0))
        pace = str(rawpace).lstrip('0:')
    return pace

def get_5k_equiv(guntime, c25k):
    total_seconds = guntime.total_seconds()
    c25k = timedelta(seconds=round(int(total_seconds) * float(c25k)))
    return c25k

def get_wrdict(asofdate, qsgender, qsage, alltime=False, getstd=False):
    wrdict = {}                                                              
    wrlist = []
    namedresult = namedtuple('nr', ['athlete', 'athlete_id', 'athlete_name', 'aseconds', 'result'])
    namedwr = namedtuple('nwr', ['rank',
                                 'athlete',
                                 'athlete_name',
                                 'avgaseconds',
                                 'bestaseconds',
                                 'secondbestaseconds',
                                 'avgkm',
                                 'd2d',
                                 'd2dred',
                                 'age',
                                 'events',
                                 'stds'])
    gender = 'FM'
    d2e = int(Config.objects.get(name='d2e').value)
    minage, maxage = Agefilter.objects.filter(slug=qsage).values_list('minage', 'maxage')[0]
    if alltime:
        startdate = datetime.strptime('1900-01-01', "%Y-%m-%d").date()
    else:
        startdate = (asofdate - timedelta(days=(d2e - 1)))       
    exclude_athletes = (Athlete                                                  
                        .objects                                                   
                        .filter(quitdate__lt=asofdate)                           
                        .values_list('id', flat=True))                             
    dbresults = (Result.objects
                .select_related()
                .filter(athlete__isguest=False,
                        event__date__gte=startdate,
                        event__date__lte=asofdate)     
                .order_by('-event__date')
               )
    c25kdict = dict(Distance.objects.values_list('id', 'c25k'))
    if getstd:
        stddict = get_stddict(asofdate)
        config_dict = dict(Config.objects.values_list('name', 'value'))
        std_colour_dict = {'A': config_dict['standard_a_colour_class'],
                           'B': config_dict['standard_b_colour_class'],
                           'C': config_dict['standard_c_colour_class']}
    else:
        stds = False
    results = []
    for result in dbresults:
        #c25k = c25kdict[result.event.distance.id]
        aseconds = result.fivek_equiv.total_seconds()
        if result.athlete.gender in qsgender:
            results.append(namedresult(result.athlete, result.athlete.id, result.athlete.name, aseconds, result))
    results = sorted(results, key=attrgetter('athlete_id', 'aseconds'))  
    #print('--------------------')
    for key, group in itertools.groupby(results, attrgetter('athlete_id')):
        thisathlete = list(group)                                                
        joindate = thisathlete[0].athlete.joindate
        athlete_age = get_age(asofdate, thisathlete[0].athlete.dob)
        if thisathlete[0].athlete_id in exclude_athletes:
            continue
        if len(thisathlete) >= 3:                                                
            #Print the events used for ranking
            #print(';{}'.format(thisathlete[0].result.event))
            #print(';{}'.format(thisathlete[1].result.event))
            #print(';{}'.format(thisathlete[2].result.event))
            if thisathlete[0].athlete.gender not in gender:     
                continue                                                         
            if qsage != 'any-age':
                if not minage <= athlete_age <= maxage:
                    continue
            if joindate > asofdate:       
                continue                                                         
            avgaseconds = sum([x.aseconds for x in thisathlete[0:3]])/3          
            avgkm = round(sum([x.result.event.distance.km for x in thisathlete[0:3]])/3,1)             
            mindate = min([ x.result.event.date for x in thisathlete[0:3] ])
            d2d = d2e - (asofdate - mindate).days                     
            d2dred = False
            if len(thisathlete) <= 3:
                d2dred = True
            bestaseconds = thisathlete[0].aseconds                               
            secondbestaseconds = thisathlete[1].aseconds                         
            events = [thisathlete[0].result.event, thisathlete[1].result.event, thisathlete[2].result.event]
            if getstd:
                stds = get_stds(stddict, thisathlete[0].athlete, std_colour_dict)
            wrlist.append(namedwr(0, thisathlete[0].athlete, thisathlete[0].athlete.name, avgaseconds, bestaseconds, secondbestaseconds, avgkm, d2d, d2dred, athlete_age, events, stds))
    wrlist = sorted(wrlist, key=attrgetter('avgaseconds', 'bestaseconds', 'secondbestaseconds', 'athlete_name'))
    count = 1                                                                    
    for a in wrlist:                                                           
        wrdict[a.athlete] = namedwr(count, a.athlete, a.athlete_name, a.avgaseconds, a.bestaseconds, a.secondbestaseconds, a.avgkm, a.d2d, a.d2dred, a.age, a.events, a.stds)  
        count += 1                                                               
    #print(len(db.connection.queries))   # number of sql queries that happened
    return wrdict                       

def wrdict2list(wrdict1, wrdict2, alltime):
    namedwr = namedtuple('nwr', ['rank', 'athlete', 'athlete_name', 'athlete_shortname', 'gender_colour_class', 'avgaseconds', 'bestaseconds', 'secondbestaseconds', 'atime', 'avgkm', 'd2d', 'd2dred', 'ti', 'tidir', 'plusminus', 'pmdir', 'minutegroup', 'glyph', 'stds'])
    config_dict = dict(Config.objects.values_list('name', 'value'))
    female_colour_class = config_dict['female_colour_class']
    male_colour_class = config_dict['male_colour_class']
    master_glyph = config_dict['master_glyph']
    wrlist = []
    for k, v in wrdict1.items():
        atime = str(timedelta(seconds=round(v.avgaseconds))).lstrip('0:')
        minutegroup = str(atime).split(':')[-2]
        split_athlete_name =  v.athlete_name.split(' ')
        athlete_shortname = '{} {}'.format(split_athlete_name[0], split_athlete_name[1][0])
        ti = False
        tidir = False
        plusminus = False
        pmdir = False
        glyph = False
        # Comment out master glyph while I decide if I like want it
        #if v.age >= 40:
        #    glyph = master_glyph
        if v.athlete.gender == 'F':
            gender_colour_class = female_colour_class
        else:
            gender_colour_class = male_colour_class
        if wrdict2:
            if v.athlete in wrdict2:
                ti = round(v.avgaseconds) - round(wrdict2[v.athlete].avgaseconds)
                if ti == 0:
                    ti = False
                else:
                    if ti > 0:
                        ti = str(timedelta(seconds=(abs(ti))))[-4:]
                        tidir = 'down'
                    else:
                        ti = str(timedelta(seconds=(abs(ti))))[-4:]
                        tidir = 'up'
                plusminus = v.rank - wrdict2[v.athlete].rank 
                if plusminus == 0:
                    plusminus = False
                else:
                    if plusminus > 0:
                        plusminus = abs(plusminus)
                        pmdir = 'down'
                    else:
                        plusminus = abs(plusminus)
                        pmdir = 'up'
            else:
                plusminus = 'N'
            if alltime:
                d2d = '-'
                d2dred = False
            else:
                d2d =v.d2d
                d2dred = v.d2dred
        else:
            d2d =v.d2d
            d2dred = v.d2dred
            plusminus = 'N'
               
        wrlist.append(namedwr(v.rank, v.athlete, v.athlete_name, athlete_shortname, gender_colour_class, v.avgaseconds, v.bestaseconds, v.secondbestaseconds, atime, v.avgkm, d2d, d2dred, ti, tidir, plusminus, pmdir, minutegroup, glyph, v.stds))
    wrlist = sorted(wrlist, key=attrgetter('avgaseconds', 'bestaseconds', 'secondbestaseconds', 'athlete_name'))
    return wrlist 
 
def getprevmonthlastday(year, month):                                            
    if month == 1:                                                               
        prevmonth = 12                                                           
        prevyear = year - 1                                                      
    else:                                                                        
        prevmonth = month - 1                                                    
        prevyear = year                                                          
    prevday = monthrange(prevyear, prevmonth)[1]                        
    prevstrdate =  '{}-{}-{}'.format(prevyear, prevmonth, prevday)               
    prevmonthlastday = datetime.strptime(prevstrdate, "%Y-%m-%d").date()
    return prevmonthlastday                     

def monthnum2name(monthnum):
    map = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'}
    return map[monthnum]

def get_age(asofdate, dob):
    if dob is None:
        age = -1
    else:
        age = relativedelta(asofdate, dob).years
    return age

def get_races(dates):
    namedrace = namedtuple('nr', ['year_date', 'city', 'date', 'weeks', 'events'])
    nameddistance = namedtuple('nd', ['abbr', 'slug'])
    today = date.today()
    all_events = Event.objects.values_list('id', flat=True)
    events_with_hpresults = Result.objects.filter(athlete__isguest=False, athlete__quitdate=None).values_list('event', flat=True).distinct()
    #events_without_results = set(all_events) - set(events_with_hpresults)
    if dates == 'Past':
        dbevents = Event.objects.select_related().filter(pk__in=set(events_with_hpresults)).order_by('-date', '-distance__km')
    else:
        dbevents = Event.objects.select_related().filter(date__gte=today).filter(hasresults=False).order_by('date', '-distance__km')
    racesdict = {}
    races = []
    events = []
    for event in dbevents:
        year_date = '{} {}'.format(event.date, event.race.name)
        if year_date in racesdict:
            racesdict[year_date] += [event,]
        else:
            racesdict[year_date] = [event,]
    for k, v in racesdict.items():
        raceitem = [k, v[0].city, v[0].date, v]
        weeks = round(((v[0].date - today).days / 7), 1)
        races.append(namedrace(raceitem[0], raceitem[1], raceitem[2], weeks, raceitem[3]))
    if dates == 'Past':
        races = sorted(races, key=attrgetter('date'), reverse=True)
    else:
        races = sorted(races, key=attrgetter('date'))
    return races

def get_recent_pbs(num):                                                            
    named_pb = namedtuple('npb', ['result', 'trunc_time'])
    pb_dict = dict(Athlete.objects                                               
               .filter(quitdate=None, isguest=False)                             
               .annotate(min_fivek_equiv=Min('result__fivek_equiv'))             
               .values_list('id', 'min_fivek_equiv'))                            
    pb_result_ids = []                                                           
    for k, v in pb_dict.items():                                                 
        pb_result = Result.objects.filter(athlete_id=k, fivek_equiv=v).order_by('event__date')
        if len(pb_result) > 0:
            pb_result_ids.append(pb_result[0].id)                                
    dbrecent_pbs = Result.objects.filter(id__in=pb_result_ids).order_by('-event__date', '?')[:num]
    recent_pbs = []
    for i in dbrecent_pbs:
        recent_pbs.append(named_pb(i, truncate_time(i.chiptime)))
    return recent_pbs              

def endursort(distances):
    sorted_distances = []
    try:
        sorted_distances.append(distances[3])
    except:
        pass
    try:
        sorted_distances.append(distances[5])
    except:
        pass
    try:
        sorted_distances.append(distances[1])
    except:
        pass
    try:
        sorted_distances.append(distances[4])
    except:
        pass
    try:
        sorted_distances.append(distances[2])
    except:
        pass
    try:
        sorted_distances.append(distances[6])
    except:
        pass
    try:
        sorted_distances.append(distances[0])
    except:
        pass
    return sorted_distances

def create_samerace_list(race):
    """ Make a list of races that are the same """
    races = [race, ]
    for i in Samerace.objects.filter(current_race=race):
        races.append(i.old_race)
    for i in Samerace.objects.filter(old_race=race):
        races.append(i.current_race)
    return races

def get_stddict(asofdate):
    stddict = {}
    dbstds = Standardcalc.objects.select_related().filter(event__date__lte=asofdate).order_by('event__date')
    for s in dbstds:
        if s.athlete in stddict:
            stddict[s.athlete].append(s)
        else:
            stddict[s.athlete] = [s,]
    return stddict

def get_stds(std_dict, athlete, std_colour_dict):
    named_std = namedtuple('ns', ['letter', 'colour', 'mouseover'])
    if athlete in std_dict:
        dist_dict = {}
        best_stds = []
        letter = sorted(std_dict[athlete], key=attrgetter('standarddef.letter'))[0].standarddef.letter
        for s in reversed(std_dict[athlete]):
            master = 'not-master'
            if s.standarddef.ismasters:
                master = 'master'
            pb = 'nopb'
            if s.ispb:
                pb = 'pb'
            this_std = '{}__{}__{}'.format(s.event.distance.slug, master, pb)
            if this_std not in dist_dict:
                dist_dict[this_std] = s
        for k, v in dist_dict.items():
            best_stds.append(v)
        best_stds = sorted(best_stds, key=attrgetter('standarddef.letter', 'standarddef.distance.km'))
        strbest_stds = []
        for b in best_stds:
            brackets = ''
            if b.ispb:
                brackets = ' (PB)'
            elif b.standarddef.ismasters:
                brackets = ' (Masters)'
            thisstr = '{} Standard: {}{}'.format(b.standarddef.letter, b.standarddef.distance.name, brackets)
            strbest_stds.append(thisstr)
        mouseover = '\n'.join(strbest_stds)
        stds = named_std(letter, std_colour_dict[letter], mouseover)
    else:
        stds = False
    return stds
