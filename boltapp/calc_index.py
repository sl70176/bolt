from .models import Indexcalc
from .views import index
import logging
logger = logging.getLogger(__name__)

def refresh_index_calculated():
    logger.info('Refreshing index_calculated table')
    response = index.index(False)
    Indexcalc(id=1, value=response.content.decode('utf-8')).save()
    logger.info('Finished calculating index')
