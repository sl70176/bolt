from .models import Athlete, Athleteracename
import logging
logger = logging.getLogger(__name__)

def index(athlete_id, athlete_name):
    """ Add name to Athleteracename if not already there """
    logger.info('Checking if racename exists for {}'.format(athlete_name))
    athlete_race_names = Athleteracename.objects.values_list('name', flat=True)
    if athlete_name in athlete_race_names:
        logger.info('{} already exists, no action required'.format(athlete_name))
    else:
        logger.info('Adding {} to racename table)'.format(athlete_name))
        Athleteracename(athlete_id=athlete_id, name=athlete_name).save()
