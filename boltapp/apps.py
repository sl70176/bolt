from django.apps import AppConfig


class BoltappConfig(AppConfig):
    name = 'boltapp'
    verbose_name = 'Boltapp Application'

    def ready(self):
        import boltapp.signals
