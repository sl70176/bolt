variable "profile" {}

variable "scrw-ip" {}

variable "sam-key" {}

variable "region" {
  default = "us-east-1"
}

variable "ami" {
  default = "ami-d5bf2caa"
}

variable "instance_type" {
  default = "t2.micro"
}

