## TODO: Billing alarms (SNS topics/subscriptions?)
## TODO: autorecovery

# Shared S3 bucket for tfstate
terraform {
  backend "s3" {
    key     = "terraform-sd.tfstate"
    bucket  = "tfstate-sl"  # DO NOT CHANGE
    profile = "sl"          # DO NOT CHANGE
    region  = "us-east-1"   # DO NOT CHANGE
  }
}

# Configure the AWS Provider
provider "aws" {
  profile = var.profile
  region  = var.region
  version = "~> 2.0"
}

# Configure key pair
resource "aws_key_pair" "sam-key" {
  key_name   = "sam-key"
  public_key = var.sam-key
}

# Configure the security group
resource "aws_security_group" "hagen-sg" {
  name = "hagen-sg"
  description = "primary security group"
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"] 
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"] 
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.scrw-ip}/32",] 
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
}
}

# Configure EC2 instance
resource "aws_instance" "hagen" {
  ami           = var.ami
  key_name      = "sam-key"
  instance_type = var.instance_type
  tags = {
    Name = "hagen"
  }
  vpc_security_group_ids = ["${aws_security_group.hagen-sg.id}"]
}

# Configure elastic ip address
resource "aws_eip" "hagen-eip" {
  instance = aws_instance.hagen.id
  vpc      = true
}

# build the CloudWatch auto-recovery alarm and recovery action
resource "aws_cloudwatch_metric_alarm" "hagen-recovery" {
  alarm_name         = "hagen-recovery"
  namespace          = "AWS/EC2"
  evaluation_periods = "3"
  period             = "60"
  alarm_description  = "This metric auto recovers EC2 instances"
  alarm_actions = ["arn:aws:automate:${var.region}:ec2:recover"]
  statistic           = "Minimum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = "0.0"
  metric_name         = "StatusCheckFailed_System"
  dimensions = {
    InstanceId = "${aws_instance.hagen.id}"
  }
}