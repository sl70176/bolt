### Install AWS CLI
```
sudo python3 -m pip install awscli
```

### Configure AWS profile
*Note: Make sure to configure a default region*
```
export PROFILE=<profile>
aws configure --profile ${PROFILE}
```

### Install Terraform
```
sudo -i
cd /usr/local/bin
wget https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip
unzip terraform_0.12.18_linux_amd64.zip
rm -f terraform_0.12.18_linux_amd64.zip
```

### Accout switch dance (avoiding downtime)
```
export PROFILE=<new-profile>
aws configure --profile ${PROFILE}
# terraform.tfvars: update profile variable to new
# main.tf: update profile part of backend key
terraform init  # do not copy existing state
terraform plan
terraform apply
# run ansible, get everything up
# move DNS
# terraform.tfvars: update profile variable to old
# main.tf: update profile part of backend key
terraform init
terraform destroy
# terraform.tfvars: update profile variable to new
# main.tf: update profile part of backend key
terraform inint
terraform plan  # to validate no changes are required

